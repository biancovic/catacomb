function encoder = GeneratePoolingEncoderIPBC(trainImages, cType, nType, r, encType, nClusters)
%Pooling encoder for Image Patch-Based Classifiers
%**********
%Synopsis:      encoder = GeneratePoolingEncoderIPBC(trainImages, cType, nType, r, encType, nClusters)
%**********
%INPUT
%trainImages:   Relative or absolute path to the folder containing the 
%               train images.
%cType:         The type of Image Patch-Based Classifer. Can be:
%                   'J'         -> Joint classifier (includes the central 
%                                                    pixel)
%                   'N'         -> Neighbourhood classifier (does not include
%                                                            the central pixel)
%nType:         The type of neighbourhood. See DrawCircle() for possible
%               values.
%r:             The radius of the neighbourhood (in pixels).
%encType:       The type of encoding. Can be:
%                   'BoW'       -> Bag-of-words encoding
%                   'Fisher'    -> Fisher vector encoding
%                   'VLAD'      -> Vector of Linearly Aggregated
%                                  Descriptors
%nClusters:     Number of clusters used to encode the features (i.e.: 
%               dimension of the dictionary)
%**********
%OUTPUT
%encoder:       The structure containing the data for the encoding. 
%               See GenerateFeatureEncoder()
%**********
%
%   Sample usage
%   --------
%      %Example: Create a bag-of-words vector encoder for a joint Image
%      Patch-Based Classifier
%      trainImages = './MyImages';
%      cType = 'J'
%      nClusters = 16;
%      nType = 'eight-point-circle';
%      r = 1;
%      encType = 'BoW';
%      encoder = GeneratePoolingEncoderIPBC(trainImages, cType, nType,...
%      r, encType, nClusters);

%References
%[1]    M. Varma and A. Zissermann, "A statistical approach to material
%       classification using image patch exemplars"; IEEE Transactions on 
%       Pattern Analysis and Machine Intelligence, 31(11):2032-2047  
    
    trainData = single([]);

    %Generate the neighbourhood
    disps = DrawCircle(r, nType);
    
    %Add the central pixel if required
    if cType == 'J'
        disps = [0,0; disps];
    end
    
    %Get the train images
    images = GetFileNames(trainImages);
    nTrainImages = size(images, 1);
    
    %Number of clusters per image
    nClustersPerImage = ones(1,nTrainImages);
    if nTrainImages > nClusters
        %Do nothing here -- see below
    else
        r = mod(nClusters, nTrainImages);
        n = (nClusters - r)/nTrainImages;
        nClustersPerImage = n*nClustersPerImage;
        nClustersPerImage(end) = nClustersPerImage(end) + r;
    end
        
    %Process the train images and collect the train data
    centers = [];
    means = [];
    covariances = [];
    priors = [];
    nImages = size(images,1);
    for i = 1:nImages
        img = [];
        imgFile = images(i,:);
        img = imread(imgFile);
        [rows cols ch] = size(img);
        
        %Convert to greyscale if required
        if ch > 1
            img = rgb2gray(img);
        end
        
        fprintf('Processing image %d of %d: %s\n', i, nImages, imgFile);
        
        %Convert to single precision and normalize to zero mean and unit
        %variance
        img = single(img);
        img = zscore(img);
        
        %Slice the image
        patches = CreateImageLayers(img, disps, 'circular');
        
        %Normalize the patches using Weber's law
        L = sqrt(sum(patches.^2,3));
        K = log(1 + L/0.003)./L;
        K = repmat(K, [1 1 size(patches, 3)]);
        patches = K .* patches;
                
        X = reshape(permute(patches, [3 2 1]),...
                            [size(patches,3) size(patches,1)*size(patches,2)]);
        
        %Generate the encoders' data
        encoder = GenerateFeatureEncoder(X, encType, nClustersPerImage(i));   
        
        switch encType
            case 'BoW'
                centers_ = vl_kmeans(X, nClustersPerImage(i));
                centers = [centers, centers_];
            case 'Fisher'
                [centers_, covariances_, priors_] = ...
                    vl_gmm(X, nClustersPerImage(i));     
                centers = [centers, centers_];
                covariances = [covariances, covariances_];
                priors = [priors, priors_];
            case 'VLAD'
                centers_ = vl_kmeans(X, nClustersPerImage(i));
                centers = [centers, centers_];
            otherwise 
                error('Unsupported encoding');
        end
    end
    
    encoder = [];
    if nTrainImages > nClusters
        %If nTrainImages > nClusters use the cluster centers as train data
        encoder = GenerateFeatureEncoder(centers, encType, nClusters);
    else
        switch encType
            case 'BoW'            
                %Define the the data-to-cluster assignments through a kd-tree
                kdtree = vl_kdtreebuild(centers) ;      
                encoder = struct('type', 'BoW',...
                                 'nclusters', nClusters,...
                                 'centers', centers,...
                                 'kdtree', kdtree);
            case 'Fisher'    
                encoder = struct('type', 'Fisher',...
                                 'nclusters', nClusters,...
                                 'means', means,...
                                 'covariances', covariances,...
                                 'priors', priors);
            case 'VLAD'            
                kdtree = vl_kdtreebuild(centers) ;      
                encoder = struct('type', 'VLAD',...
                                 'nclusters', nClusters,...
                                 'centers', centers,...
                                 'kdtree', kdtree);
        otherwise 
            error('Unsupported encoding');
        end
    end
end

