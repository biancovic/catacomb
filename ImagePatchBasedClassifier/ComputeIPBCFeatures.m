function F = ComputeIPBCFeatures(img, cType, nType, r, encoder, optArgs)
%Texture features based on the Image Patch-Based Classifier 
%**********
%Synopsis:      F = ComputeIPBCFeatures(img, cType, nType, r, encoder, optArgs)
%**********
%INPUT:
%img:           The input image.
%cType:         The type of Image Patch-Based Classifer. Can be:
%                   'J'         -> Joint classifier (includes the central 
%                                                    pixel)
%                   'N'         -> Neighbourhood classifier (does not include
%                                                            the central pixel)
%nType:         The type of neighbourhood. See DrawCircle() for possible
%               values.
%r:             The radius of the neighbourhood (in pixels).
%encoder:       A pre-trained encoder -- see GeneratePoolingEncoderIPBC().
%optArgs:       Optional arguments for feature encoding -- see
%               EncodeFeatures().
%**********
%OUTPUT:
%features:      The feature vector.  
%**********
%
%   Sample usage
%   --------
%
%      %Example 1: Compute Image Patch-Based features encoded with BoW pooling
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      cType = 'J';                               %Joint classifier
%      nType = 'eight-point-square';              %3x3 window
%      r = 1;
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      encType = 'BoW';                           %Type of encoder
%      BoWEncoder = GeneratePoolingEncoderIPBC(trainImages, cType,...
%                                              nType, r, encType, nClusters);
%      F = ComputeIPBCFeatures(img, cType, nType, r, BoWencoder, []);
%
%      %Example 2: Compute Image Patch-Based features encoded with Fisher 
%      %vector pooling
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      cType = 'N';                               %Neighbourhood classifier
%      nType = 'eight-point-square';              %3x3 window
%      r = 1;
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      encType = 'Fisher';                        %Type of encoder
%      optArgs = {'Improved'};
%      IFVEncoder = GeneratePoolingEncoderIPBC(trainImages, cType,...
%                                              nType, r, encType, nClusters);
%      F = ComputeIPBCFeatures(img, cType, nType, r, IFVEncoder, optArgs);
%
%      %Example 3: Compute Image Patch-Based features encoded VLAD pooling
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      cType = 'N';                               %Neighbourhood classifier
%      nType = 'eight-point-square';              %3x3 window
%      r = 1;
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      encType = 'VLAD';                          %Type of encoder
%      optArgs = {'NormalizeComponents'};
%      VLADEncoder = GeneratePoolingEncoderIPBC(trainImages, cType,...
%                                               nType, r, encType, nClusters);
%      F = ComputeIPBCFeatures(img, cType, nType, r, VLADEncoder, optArgs);

%References
%[1]    M. Varma and A. Zissermann, "A statistical approach to material
%       classification using image patch exemplars"; IEEE Transactions on 
%       Pattern Analysis and Machine Intelligence, 31(11):2032-2047  

    [rows cols ch] = size(img);
    
    %Convert to grey-scale if required
    if ch >  1
        img = rgb2gray(img);
    end
    
    %Generate the neighbourhood
    disps = DrawCircle(r, nType);
    
    %Add the central pixel if required
    if cType == 'J'
        disps = [0,0; disps];
    end
    
    %Convert to single precision and normalize to zero mean and unit
    %variance
    img = single(img);
    img = zscore(img);
        
    %Slice the image
    patches = CreateImageLayers(img, disps, 'circular');
        
    %Normalize the patches using Weber's law
    L = sqrt(sum(patches.^2,3));
    K = log(1 + L/0.003)./L;
    K = repmat(K, [1 1 size(patches, 3)]);
    patches = K .* patches;
                
    X = reshape(permute(patches, [3 2 1]),...
                        [size(patches,3) size(patches,1)*size(patches,2)]);
        
    %Encode the patches using the encoder selected
    F = EncodeFeatures(X, encoder, optArgs);    
    F = F';
end



