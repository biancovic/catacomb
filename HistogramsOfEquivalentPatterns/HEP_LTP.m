function [codemap, h] = HEP_LTP(img, r, ftrType, crcType, fltType)
%Local Ternary Patterns
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which LTP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For details see Ref. [2].
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The map of LTP codes (codeword image)
%h:         The normalised histogram of LTP codes (sums one)
%**********
%
%   Sample usage
%   ------------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_LTP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_LTP(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]   Tan, X., Triggs, B.: Enhanced local texture feature sets for face
%      recognition under difficult lighting conditions. In: Analysis and
%      Modelling of Faces and Gestures. Lecture Notes in Computer
%      Science, vol. 4778, pp. 168�182. Springer, Berlin (2007).
%[2]   Bianconi, F., Gonz�lez, E.
%      Counting local n-ary patterns
%      (2019) Pattern Recognition Letters, 117, pp. 24-29. 

    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    L = CreateImageLayers(img,disps,fltType);
    
    %Compute the LTP codes
    mask = 2.^(0:size(L,3)-1);              %Mask of weights
    codes = 0:sum(ones(size(mask)).*mask);  %Possible codes
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(L,1) size(L,2) 1]);
    I_copy = repmat(img, [1 1 size(L,3)]);
    
    %Compare the central pixel with the peripheral one
    %Lower: Assign 1 if the central pixel is lower than the peripheral one
    %and 0 if not
    %Upper: Assign 1 if the central pixel is greater than the peripheral
    %one and 0 if not
    comparisonsLower = zeros(size(L));
    comparisonsUpper = zeros(size(L));
    
    comparisonsLower(I_copy < L) = 1;
    comparisonsUpper(I_copy > L) = 1;
    
    %Compute the codemaps
    codemapLower = sum(comparisonsLower.*mask,3);
    codemapUpper = sum(comparisonsUpper.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(L,3);
    nColours = 2;
    nType = 'PERI';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN')
        pType = 'all';
        [codemapLower, codes] = PostprocessCodes(codemapLower, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapUpper, codes] = PostprocessCodes(codemapUpper, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        [codemapLower, codes] = PostprocessCodes(codemapLower, nBeads, nColours,...
            'CN', 'none', nType, pType);
        [codemapUpper, codes] = PostprocessCodes(codemapUpper, nBeads, nColours,...
            'CN', 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
        
    codemap = cat(3, codemapLower, codemapUpper);
    
    %Compute the normalised histograms
    hLower = hist(codemapLower(:), codes)/numel(codemapLower);
    hUpper = hist(codemapUpper(:), codes)/numel(codemapUpper);
    
    %Concatenate the two histograms
    h = [hLower, hUpper]/2;
end