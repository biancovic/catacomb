function [codemap,h] = HEP_LCVBP(img, r, ftrType, crcType, fltType)
%Local Colour Vector Binary Patterns
%**********
%Synopsis:  HEP_LCVBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which CNP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The maps of LCVBP codes (codeword image s). An N x M matrix
%h:         The normalised histogram of LCVBP codes (sums one)
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_LCVBP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_LCVBP(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]   S.H. Lee, J.Y. Choi, Y.M. Ro and K.N. Plataniotis; "Local color 
%      vector binary patterns from multichannel face images for face 
%      recognition"; IEEE Transactions on Image Processing 21(4):2347-2353,
%      2012

% Version published in 2016 by Francesco Bianconi 
% Department of Engineering, UniversitÓ degli Studi di Perugia, Italy

    %Colour norm patterns
    [codemapCNP,hCNP] = HEP_CNP(img, r, ftrType, crcType, fltType);
    
    %Colour anguler patterns
    [codemapCAP,hCAP] = HEP_CAP(img, r, ftrType, crcType, fltType);
    
    %Put everything together
    codemap = cat(3, codemapCNP, codemapCAP);
    h = [hCNP, 3*hCAP]/4;

    
end