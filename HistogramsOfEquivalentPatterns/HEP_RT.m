function [codemap,h] = HEP_RT(img, r, crcType, fltType)
%An implementation of Rank Transform based on digital circles
%**********
%Synopsis:  [codemap,h] = HEP_RT(img, r, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which RT is computed.
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The map of RT codes (codeword image)
%h:         The normalised histogram of RT codes (sums one)
%**********
%NOTE: Rank transform features are by definition invariant to rotations and
%reflections
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_RT(img, 2, 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_RT(img, 2, 'Andres', 'symmetric');

% Version published in 2016 by Francesco Bianconi 
% Department of Engineering, UniversitÓ degli Studi di Perugia, Italy

    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    L = CreateImageLayers(img,disps,fltType);
    
    %Compute the RT codes
    mask = 2.^(0:size(L,3)-1);  %Mask of weights
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(L,1) size(L,2) 1]);
    I_copy = repmat(img, [1 1 size(L,3)]);
    codemap = sum((I_copy < L),3);
    
    %Compute the normalised histogram
    nFeats = size(L,3);     %Number of features 
    h = hist(codemap(:), 0:nFeats)/numel(codemap);
end