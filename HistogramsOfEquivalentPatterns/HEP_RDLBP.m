function [codemap,h,codes] = HEP_RDLBP(img, r, ftrType, crcType, fltType)
%An implementation of Radial-Differences Local Binary Patterns based on digital circles
%**********
%Synopsis:  HEP_RDLBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radii of the two concentric circles (inner, outer) used to compute the 
%           radial differences. A 1 x 2 array of integers. Note that the
%           circles need to have the same number of points.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The map of LBP codes (codeword image)
%h:         The normalised histogram of RDLBP codes (sums one)
%codes:     The codes of the local patterns (dictionary)
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Radius of the inner circle = 2px, radius of the outer circle = 3px,
%      %directional features, circular repetition across
%      %the image borders, approximated eight-point digital circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_RDLBP(img, [2 3], 'dir', 'eight-point-circle', 'circular');
%
%      %Example 2
%      %Radius of the inner circle = 3px, radius of the outer circle = 5px,
%      %rotation-invariant features, circular repetition across
%      %the image borders, approximated eight-point digital circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_RDLBP(img, [3 5], 'CN', 'Andres', 'circular');
%
%References
%[1]    L. Liu, L. Zhao, Y. long G. Kuang and P. Fieguth; Extended local
%       binary patterns for texture classification, Image and Vision Computing
%       30 (2012):86-89 

% Version published in 2016 by Francesco Bianconi 
% Department of Engineering, UniversitÓ degli Studi di Perugia, Italy

    %Generate the neighbourhoods
    dispsInner = DrawCircle(r(1), crcType);
    dispsOuter = DrawCircle(r(2), crcType);
    
    %Make sure the inner and outer circle have the same number of points
    if size(dispsInner, 1) ~= size(dispsInner, 1)
        error('The inner and outer circle must have the same number of points');
    end
    
    %Layerize the input image
    lInner = CreateImageLayers(img,dispsInner,fltType);
    lOuter = CreateImageLayers(img,dispsOuter,fltType);
    
    %Compute the LBP codes
    mask = 2.^(0:size(lInner,3)-1);                  %Mask of weights
    codes = 0:sum(ones(size(mask)).*mask);      %Possible codes
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(lInner,1) size(lInner,2) 1]);
    img = repmat(img, [1 1 size(lInner,3)]);
    comparisons = (lInner <= lOuter);
    codemap = sum(comparisons.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(lInner,3);
    nColours = 2;
    nType = 'PERI';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN') 
        pType = 'all';
        [codemap, codes] = PostprocessCodes(codemap, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        [codemap, codes] = PostprocessCodes(codemap, nBeads, nColours,...
            'CN', 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
    
    %Compute the normalised histogram
    h = hist(codemap(:), codes)/numel(codemap);
    a = 0;

end