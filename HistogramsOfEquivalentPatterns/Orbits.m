function [patterns, labels, GInvLabels, nOrbits] = Orbits(N,M,G,oType,nType,pType)
%Generate the possible patterns and group-invariant patterns for a
%neighbourhood of N points over an alphabet of M symbols.
%**********
%Synopsis: [patterns, labels, GInvLabels, nOrbits] = Orbits(N,M,G,oType,nType,pType) 
%**********
%INPUT:
%N:              The number of points in the neighbourhood
%M:              The size of the alphabet 
%G:              The tranformation group acting on the set of points. Can be:
%                   'CN'    -> the cyclic group    (necklaces)
%                   'DN'    -> the dihedral group  (bracelets)
%                   'none'  -> no transformation
%oType:          Constraints on the values
%                   'none'      -> no constraints
%                   'strong'    -> M = N and no two beads can have the same
%                                  symbol (strong ordering)
%                   'allsymbs'  -> Each symbol of the alphabet must appear 
%                                  at least once 
%nType:          The geometric arrangement. Can be:
%                   'PERI' -> each X1,...,Xi,...XN ideally represents the vertex 
%                             of a regular polygon
%                   'FULL' -> the first element of X, i.e. X1 ideally
%                             represents the center of a polygon and the other
%                             X2,...,Xi,...XN elements the vertices
%pType:         The type of patterns to be considered. Can Be:
%                   'all'  -> all the patterns are considered.
%                   'uni'  -> only uniform patterns are considered. All the 
%                             non-uniform patterns are clustered in one bin.
%                             Uniform patterns are those having at most two
%                             transitions in the periphery [1].
%**********
%OUTPUT:
%patterns:              All the possible patterns. Arrays of integers in
%                       the range [0,N-1]. Each element represents the value 
%                       (symbol) of the corresponding point.
%labels:                Unique (non-group-invariant) labels of the patterns
%GInvLabels:            The group-invariant labels 
%nOrbits:               The number of orbits under the action of G
%**********
%
    
    %Generate the patterns
    patterns = [];
    REP = [];
    switch oType
        case 'allsymbs'
            patterns = Orbits(N,M,G,'none',nType,pType);
            nPatterns = size(patterns,1);
            alphabet = 0:M-1;
            toBeRemoved = zeros(nPatterns,1);
            for i = 1:nPatterns
                intrsc = intersect(patterns(i,:),alphabet);
                if numel(intrsc) ~= M
                    toBeRemoved(i) = i;
                end
            end
            toBeRemoved(toBeRemoved == 0) = [];
            patterns(toBeRemoved,:) = [];
        case 'none'
            decORD = 0:M^N-1;
            binORD = dec2base(decORD',M,N);
            for i = 1:N
                patterns = [patterns, str2num(binORD(:,i))];
            end
        case 'strong'
            [~, vec] = RankQuantization(N, M);
            for v = 1:size(vec,1)
                levels = unique(vec(v,:));
                reps = [];
                for l = 1:numel(levels)
                    reps(l) = sum(vec(v,:) == levels(l));
                end
                patterns = [patterns; PermutationsWithReplicates(levels, reps)];
            end
        case 'weak'
            patterns = zeros(1, N);
            for i = 2:N
                patterns_ = Orbits(N,i,G,'allsymbs',nType,pType);
                patterns = [patterns; patterns_];
            end
            patterns = unique(patterns, 'rows');
        otherwise
            error('Type of ordering not supported');
    end
    
    %Compute the representatives
    mask = M.^(0:N-1);
    mask = repmat(mask, [size(patterns,1) 1]);
    tentativeRepresentatives = sum(patterns.*mask,2); 
    switch G
        case 'none'
            REP = tentativeRepresentatives;
        case 'CN'
            switch nType
                case 'PERI'
                    for n = 1:N-1
                        tentativeRepresentatives = cat(2,...
                            tentativeRepresentatives,...
                            sum(circshift(patterns, [0 n]).*mask,2));
                    end
                    REP = min(tentativeRepresentatives,[],2);
                case 'FULL'
                    for n = 1:N-2
                        tentativeRepresentatives = cat(2,...
                            tentativeRepresentatives,...
                            sum([patterns(:,1) circshift(patterns(:,2:end), [0 n])].*mask,2));
                    end
                    REP = min(tentativeRepresentatives,[],2);
                otherwise
                    error('Type of neighbourhood not supported');  
            end
        case 'DN'
            switch nType
                case 'PERI'
                    %Rotations (circular shifts)
                    for n = 1:N-1
                        tentativeRepresentatives = cat(2,...
                            tentativeRepresentatives,...
                            sum(circshift(patterns, [0 n]).*mask,2));
                    end
                    %Reflections
                    for n = 0:N-1
                        tentativeRepresentatives = cat(2,...
                            tentativeRepresentatives,...
                            sum(fliplr(circshift(patterns, [0 n])).*mask,2));
                    end
                    REP = min(tentativeRepresentatives,[],2);
                case 'FULL'
                    %Rotations (circular shifts)
                    for n = 1:N-2
                        tentativeRepresentatives = cat(2,...
                            tentativeRepresentatives,...
                            sum([patterns(:,1) circshift(patterns(:,2:end), [0 n])].*mask,2));
                    end
                    %Reflections
                    for n = 0:N-2
                        tentativeRepresentatives = cat(2,...
                            tentativeRepresentatives,...
                            sum([patterns(:,1) fliplr(circshift(patterns(:,2:end), [0 n]))].*mask,2));
                    end
                    REP = min(tentativeRepresentatives,[],2);
                otherwise
                error('Type of neighbourhood not supported');
            end
        otherwise
            error('Transformation group not supported')
    end
    
    %Compute the motifs labels
    labels = sum(mask.*patterns,2);
    
    %Process uniform patterns if required
    switch pType
        case 'all'
            %Do nothing
        case 'uni'
            %Compute the number of transitions in the periphery
            switch nType
                case 'PERI'
                    patterns = cat(3, patterns, circshift(patterns,1,2));
                case 'FULL'
                    patterns = cat(3, patterns(:,2:end), circshift(patterns(:,2:end),1,2));
                otherwise
                    error('Type of neighbourhood not supported');
            end
            nTransitions = sum(patterns(:,:,1) ~= patterns(:,:,2),2);
            uPatterns = (nTransitions <= 2);        %The uniform patterns
            
            %Create one orbit for all the non-uniform patterns
            REP_uni = REP(uPatterns);
            labels_uni = unique(REP_uni);
            REP(~uPatterns) = max(labels_uni) + 1;
        otherwise
            error('Pattern type unsupported');
    end  

    patterns = patterns(:,:,1);
    
    %Compute the G-invariant labels
    [orbits,~,GInvLabels] = unique(REP);
    GInvLabels = GInvLabels - 1;
    nOrbits = numel(orbits);
        
end