function [codemap,h,codes] = HEP_CLBP_C(img, r, ftrType, crcType, fltType)
%Completed Local Binary Patterns 'C'
%**********
%Synopsis:  HEP_CLBP_C(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which CLBP_C is computed.
%ftrType:   Unused
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The map of CLBP_C codes (codeword image)
%h:         The normalised histogram of CLBP_C codes (sums one)
%codes:     The possible codes
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_CLBP_C(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_CLBP_C(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]   Guo, Z., Zhang, L., Zhang, D.
%      A completed modeling of local binary pattern operator for 
%      texture classification (2010) IEEE Transactions on Image 
%      Processing, 19 (6), art. no. 5427137, pp. 1657-1663.
%[2]   Bianconi, F., Gonz�lez, E.
%      Counting local n-ary patterns
%      (2019) Pattern Recognition Letters, 117, pp. 24-29. 

    
    %Compute the threshold (average grey-scale value)
    T = mean(img(:));
    
    %Compute the CLBP_C codes
    codes = [0, 1];
    codemap = (img <= T);
        
    %Compute the normalised histogram
    h = hist(codemap(:), codes)/numel(codemap);

end