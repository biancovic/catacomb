function [codemap,h] = HEP_TS(img, r, ftrType, crcType, fltType)
%An implementation of Texture Spectrum based on digital circles
%**********
%Synopsis:  [codemap,h] = HEP_TS(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which TS is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The map of TS codes (codeword image)
%h:         The normalised histogram of TS codes (sums one)
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_TS(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_TS(img, 2, 'ri', 'Andres', 'symmetric');

% Version published in 2016 by Francesco Bianconi 
% Department of Engineering, UniversitÓ degli Studi di Perugia, Italy

    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    L = CreateImageLayers(img,disps,fltType);
    
    %Compute the TS codes
    mask = 3.^(0:size(L,3)-1);                  %Mask of weights
    codes = 0:(3^size(L,3))-1;                  %Possible codes
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(L,1) size(L,2) 1]);
    I_copy = repmat(img, [1 1 size(L,3)]);
    
    %Compare the central pixel with the peripheral one
    %Assign 0, 1 or 2 if the central pixel is lower than, equal to or
    %greater than the peripheral one
    comparisons = zeros(size(L));
    comparisons(I_copy < L) = 0;
    comparisons(I_copy == L) = 1;
    comparisons(I_copy > L) = 2;
    
    %Compute the codemap
    codemap = sum(comparisons.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(L,3);
    nColours = 3;
    nType = 'PERI';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN') 
        pType = 'all';
        [codemap, codes] = PostprocessCodes(codemap, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        [codemap, codes] = PostprocessCodes(codemap, nBeads, nColours,...
            'CN', 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
    
    %Compute the normalised histogram
    h = hist(codemap(:), codes)/numel(codemap);
end