function [codemap,h] = HEP_oRGBLBP(img, r, ftrType, crcType, fltType)
%Hybrid Color Local Binary Patterns
%**********
%Synopsis:  HEP_oRGBLBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which oRGBLBP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The maps of oRGBLBP codes (codeword images). An N x M x 3 matrix
%           arranged as follows:
%               Layer 1 -> intra-channel codes (L)
%               Layer 2 -> intra-channel codes (C1)
%               Layer 3 -> intra-channel codes (C2)
%h:         The concatenated histogram (sums one).
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_oRGBLBP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_oRGBLBP(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]    Banerji, S., Verma, A. and Liu, C.; Novel color LBP descriptors for 
%       scene and image texture classification; Proceedings of the 2011 
%       International Conference on Image Processing, Computer Vision, and 
%       Pattern Recognition, IPCV 2011, 2, pp. 537-543, 2011

    %Convert to double
    img = double(img);

    %Separate the channels
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
    
    %Converto to the L-C1-C2 colour space
    L   = 0.2990 * imgR + 0.5870 * imgG + 0.1140 * imgB;
    C1  = 0.5000 * imgR + 0.5000 * imgG - 1.0000 * imgB;
    C2  = 0.8660 * imgR - 0.8660 * imgG - 0.0000 * imgB;
    
    %Compute the intra-channel codemaps and histograms (DR, DG, DB)
    [codemapL, histL] = HEP_LBP(L, r, ftrType, crcType, fltType);
    [codemapC1, histC1] = HEP_LBP(C1, r, ftrType, crcType, fltType);
    [codemapC2, histC2] = HEP_LBP(C2, r, ftrType, crcType, fltType);
        
    %Put everything together
    codemap = cat(3, codemapL,  codemapC1,  codemapC2);
    h = [histL, histC1, histC2];
    
    %Normalise histogram to sum one
    h = h/3;
end