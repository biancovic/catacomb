function [codemap,h] = HEP_IOCLBP(img, r, ftrType, crcType, fltType)
%Improved Opponent Color Local Binary Patterns
%**********
%Synopsis:  HEP_IOCLBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which IOCLBP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For details see Ref. [2].
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The maps of IOCLBP codes (codeword image s). An N x M x 6 matrix
%           arranged as follows:
%               Layer 1 -> intra-channel codes (R)
%               Layer 2 -> intra-channel codes (G)
%               Layer 3 -> intra-channel codes (B)
%               Layer 4 -> inter-channel codes (R/G)
%               Layer 5 -> inter-channel codes (R/B)
%               Layer 6 -> inter-channel codes (G/B)
%h:         The normalised histogram of LBP codes (sums one)
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_IOCLBP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_IOCLBP(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]   Bianconi, F., Bello-Cerezo, R., Napoletano, P.
%      Improved opponent color local binary patterns: An effective local 
%      image descriptor for color texture classification
%      (2018) Journal of Electronic Imaging, 27 (1), art. no. 011002
%[2]   Bianconi, F., Gonz�lez, E.
%      Counting local n-ary patterns
%      (2019) Pattern Recognition Letters, 117, pp. 24-29. 

    %Separate the channels
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
    
    %Compute the intra-channel codemaps and histograms
    [codemapR, histR, codes] = HEP_ILBP(imgR, r, ftrType, crcType, fltType);
    [codemapG, histG] = HEP_ILBP(imgG, r, ftrType, crcType, fltType);
    [codemapB, histB] = HEP_ILBP(imgB, r, ftrType, crcType, fltType);
    
    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    LR = CreateImageLayers(imgR,disps,fltType);
    LR = cat(3, LR, imgR);
    LG = CreateImageLayers(imgG,disps,fltType);
    LG = cat(3, LG, imgG);
    LB = CreateImageLayers(imgB,disps,fltType);
    LB = cat(3, LB, imgB);
    averageR = repmat(mean(LR,3), [1 1 size(disps,1) + 1]);
    averageG = repmat(mean(LG,3), [1 1 size(disps,1) + 1]);
    averageB = repmat(mean(LB,3), [1 1 size(disps,1) + 1]);
    
    %Define the weight mask
    mask = 2.^(0:size(disps,1));                 
    codes = 0:sum(ones(size(mask)).*mask);     
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(img,1) size(img,2) 1]);
    
    %Compute the inter-channel codemaps R/G, R/B and G/B   
    comparisonsRG = (LR <= averageG);
    codemapRG = sum(comparisonsRG.*mask,3);
    comparisonsRB = (LR <= averageB);
    codemapRB = sum(comparisonsRB.*mask,3);
    comparisonsGB = (LG <= averageB);
    codemapGB = sum(comparisonsGB.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(disps,1) + 1;
    nColours = 2;
    nType = 'FULL';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN')
        pType = 'all';
        [codemapRG, codes] = PostprocessCodes(codemapRG, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapRB, codes] = PostprocessCodes(codemapRB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapGB, codes] = PostprocessCodes(codemapGB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        [codemapRG, codes] = PostprocessCodes(codemapRG, nBeads, nColours,...
            'CN', 'none', nType, pType);
        [codemapRB, codes] = PostprocessCodes(codemapRB, nBeads, nColours,...
            'CN', 'none', nType, pType);
        [codemapGB, codes] = PostprocessCodes(codemapGB, nBeads, nColours,...
            'CN', 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
    
    %Compute and concatenate the normalised histogram
    histRG = hist(codemapRG(:), codes)/numel(codemapRG);
    histRB = hist(codemapRB(:), codes)/numel(codemapRB);
    histGB = hist(codemapGB(:), codes)/numel(codemapGB);
    
    %Put everything together
    codemap = cat(3, codemapR,  codemapG,  codemapB,...
                     codemapRG, codemapRB, codemapGB);
    h = [histR, histG, histB, histRG, histRB, histGB];
    
    %Normalise hitogram to sum one
    h = h/6;
end