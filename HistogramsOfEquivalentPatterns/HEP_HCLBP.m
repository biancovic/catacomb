function [codemap,h] = HEP_HCLBP(img, r, ftrType, crcType, fltType)
%Hybrid Color Local Binary Patterns
%**********
%Synopsis:  HEP_HCLBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB colour image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which HCLBP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For details see Ref. [2].
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The maps of HCLBP codes (codeword images). An N x M x 4 matrix
%           arranged as follows:
%               Layer 1 -> intra-channel codes (R)
%               Layer 2 -> intra-channel codes (G)
%               Layer 3 -> intra-channel codes (B)
%               Layer 4 -> HCLBP (DH)
%h:         The concatenated histogram (sums one).
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_HCLBP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_HCLBP(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]    Fekriershad, S. and Tajeripour F.; Color texture classification
%       based on proposed impulse-noise resistant color local binary patterns and
%       significant points selection algorithm; Sensor Review, 37(1):33-42,
%       2017
%[2]    Bianconi, F., Gonz�lez, E.
%       Counting local n-ary patterns
%       (2019) Pattern Recognition Letters, 117, pp. 24-29. 

    %Separate the channels
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
    
    %Compute the intra-channel codemaps and histograms (DR, DG, DB)
    [codemapR, histR, codes] = HEP_LBP(imgR, r, ftrType, crcType, fltType);
    [codemapG, histG] = HEP_LBP(imgG, r, ftrType, crcType, fltType);
    [codemapB, histB] = HEP_LBP(imgB, r, ftrType, crcType, fltType);
    
    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Convert to gray-scale to compute the DG channel
    imgGS = rgb2gray(img);
    
    %Layerize the input image
    L = CreateImageLayers(imgGS,disps,fltType);
    
    %Compute the local significant values (LSV)
    imgGS = repmat(imgGS, [1 1 size(disps,1)]);
    LSV = mean(L - imgGS,3);
    
    %Compute the global average of neighbourhood differences (GSV)
    GSV = mean(LSV(:));
    
    %Compute the map of valid points (LSV > GSV)
    validPoints = find(LSV > GSV);
    
    %Compute the map of LBP codes and retain the valid points only
    %Mark the invalid points with '-1'
    [codemapGS] = HEP_LBP(imgGS(:,:,1), r, ftrType, crcType, fltType);
    codemapGS(LSV <= GSV) = -1;
    histDG = hist(codemapGS(validPoints), codes)/numel(validPoints);
        
    %Put everything together
    codemap = cat(3, codemapR,  codemapG,  codemapB, codemapGS);
    h = [histR, histG, histB, histDG];
    
    %Normalise histogram to sum one
    h = h/4;
end