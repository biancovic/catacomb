function [angleMap,h] = HEP_LCC(img, r, aLimits, nBins, crcType, fltType)
%Local Colour Contrast
%**********
%Synopsis:  HEP_LCC(img, r, aLimits, nBins, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which LCC is computed.
%aLimits    Angular interval over which the angular distribution is
%           to be computed. This is an array of two elements representing the
%           lower and upper bounds of the interval, e.g. [-pi,pi].
%nBins:     Number of bins used to quantize the colour angular differences
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%angleMap:  The map of the colour angular differences. An N x M matrix.
%h:         The normalised histogram of the colour angular differences 
%           (sums one)
%**********
%
%   Sample usage
%   ------------
%      %Example
%      %Resolution = 2 pixels, distribution between -180� and 180�,
%      %number of bins = 256, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [angleMap, h] = HEP_LCC(img, 2, [-pi,pi], 256, 'Andres', 'circular');
%
%----------
%References
%[1]   C. Cusano, P. Napoletano and R. Schettini; "Combining local binary 
%      patterns and local contrast for texture classification under varying 
%      illumination"; Journal of the Optical Society of America A 31(7):1453-1461,
%      2014

    %Convert to double
    img = double(img);
    [rows cols ch] = size(img);

    %Separate the channels
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
        
    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    LR = CreateImageLayers(imgR,disps,fltType);
    LG = CreateImageLayers(imgG,disps,fltType);
    LB = CreateImageLayers(imgB,disps,fltType);
    
    %Compute the average colour 
    avgR = mean(LR,3);
    avgG = mean(LG,3);
    avgB = mean(LB,3);
    avg = cat(3, avgR, avgG, avgB);

    %**********************************
    %**** Compute the colour angle ****
    %**********************************
    img = reshape(permute(img, [3 2 1]), [3 rows*cols]);
    avg = reshape(permute(avg, [3 2 1]), [3 rows*cols]);
    dotProd = dot(img, avg);
    dotProd = vec2mat(dotProd, cols);
    normImg = sqrt(sum(img.^2,1));
    normAvg = sqrt(sum(avg.^2,1));
    normImg = vec2mat(normImg, cols);
    normAvg = vec2mat(normAvg, cols);
    acosArg = dotProd./(normImg.*normAvg);
    
    %Cut values >= 1 and <= -1 to avoid numerical problems
    acosArg(acosArg >= 1) = 1;
    acosArg(acosArg <= -1) = -1;
    
    angleMap = acos(acosArg);
    
    %Remove NaNs
    angleMap(isnan(angleMap)) = 0;
    %**********************************
    %**********************************
    %**********************************
    
    %Compute the histogram
    histEdges = linspace(aLimits(1),aLimits(2),nBins+1);
    h = histcounts(angleMap(:),histEdges)/numel(angleMap);
    
end