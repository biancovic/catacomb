function [codemap,h] = HEP_CAP(img, r, ftrType, crcType, fltType)
%Colour Angular Patterns
%**********
%Synopsis:  HEP_CAP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which CNP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For details see Ref. [2].
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The maps of CAP codes (codeword image s). An N x M matrix
%h:         The normalised histogram of CAP codes (sums one)
%**********
%
%   Sample usage
%   ------------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_CAP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_CAP(img, 2, 'CN', 'Andres', 'symmetric');
%
%
%----------
%References
%[1]   S.H. Lee, J.Y. Choi, Y.M. Ro and K.N. Plataniotis; "Local color 
%      vector binary patterns from multichannel face images for face 
%      recognition"; IEEE Transactions on Image Processing 21(4):2347-2353,
%      2012
%[2]   Bianconi, F., Gonz�lez, E.
%      Counting local n-ary patterns
%      (2019) Pattern Recognition Letters, 117, pp. 24-29. 


    %Convert to double
    img = double(img);

    %Separate the channels
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
        
    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    LR = CreateImageLayers(imgR,disps,fltType);
    LG = CreateImageLayers(imgG,disps,fltType);
    LB = CreateImageLayers(imgB,disps,fltType);
    
    %Compute the colour angle of the peripheral pixels
    anglePeriRG = atan2(LR, LG);
    anglePeriRB = atan2(LR, LB);
    anglePeriGB = atan2(LG, LB);
    
    imgR = repmat(imgR, [1 1 size(disps,1)]);
    imgG = repmat(imgG, [1 1 size(disps,1)]);
    imgB = repmat(imgB, [1 1 size(disps,1)]);
    
    %Compute the colour angle of the central pixel
    angleCenterRG = atan2(imgR, imgG);
    angleCenterRB = atan2(imgR, imgB);
    angleCenterGB = atan2(imgG, imgB);
    
    %Define the weight mask
    mask = 2.^(0:size(disps,1)-1);                 
    codes = 0:sum(ones(size(mask)).*mask);     
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(img,1) size(img,2) 1]);
     
    %Compute the inter-channel codemaps R/G, R/B and G/B   
    comparisonsRG = (angleCenterRG <= anglePeriRG);
    codemapRG = sum(comparisonsRG.*mask,3);
    comparisonsRB = (angleCenterRB <= anglePeriRB);
    codemapRB = sum(comparisonsRB.*mask,3);
    comparisonsGB = (angleCenterGB <= anglePeriGB);
    codemapGB = sum(comparisonsGB.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(disps,1);
    nColours = 2;
    nType = 'PERI';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN') 
        pType = 'all';
        [codemapRG, codes] = PostprocessCodes(codemapRG, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapRB, codes] = PostprocessCodes(codemapRB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapGB, codes] = PostprocessCodes(codemapGB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        ftrType = 'CN';
        [codemapRG, codes] = PostprocessCodes(codemapRG, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapRB, codes] = PostprocessCodes(codemapRB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapGB, codes] = PostprocessCodes(codemapGB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
    
    %Compute and concatenate the normalised histogram
    histRG = hist(codemapRG(:), codes)/numel(codemapRG);
    histRB = hist(codemapRB(:), codes)/numel(codemapRB);
    histGB = hist(codemapGB(:), codes)/numel(codemapGB);
    
    %Put everything together
    codemap = cat(3, codemapRG, codemapRB, codemapGB);
    h = [histRG, histRB, histGB];
    
    %Normalise hitogram to sum one
    h = h/3;
    
end