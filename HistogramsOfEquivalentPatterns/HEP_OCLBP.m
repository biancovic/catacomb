function [codemap,h] = HEP_OCLBP(img, r, ftrType, crcType, fltType)
%Opponent Color Local Binary Patterns
%**********
%Synopsis:  HEP_OCLBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An N x M x 3 RGB image.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which OCLBP is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The maps of OCLBP codes (codeword image s). An N x M x 6 matrix
%           arranged as follows:
%               Layer 1 -> intra-channel codes (R)
%               Layer 2 -> intra-channel codes (G)
%               Layer 3 -> intra-channel codes (B)
%               Layer 4 -> inter-channel codes (R/G)
%               Layer 5 -> inter-channel codes (R/B)
%               Layer 6 -> inter-channel codes (G/B)
%h:         The normalised histogram of LBP codes (sums one)
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_OCLBP(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('peppers.png');
%      [codemap, h] = HEP_OCLBP(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]    T. M�enp�� and M. Pietik�inen, Texture analysis with local binary pat-
%       terns in Handbook of Pattern Recognition and Computer Vision, 3rd
%       ed., C. H. Chen and P. S. P. Wang, Eds., pp. 197�216, World Scientific
%       Publishing, Singapore (2005)

    %Separate the channels
    imgR = img(:,:,1);
    imgG = img(:,:,2);
    imgB = img(:,:,3);
    
    %Compute the intra-channel codemaps and histograms
    [codemapR, histR, codes] = HEP_LBP(imgR, r, ftrType, crcType, fltType);
    [codemapG, histG] = HEP_LBP(imgG, r, ftrType, crcType, fltType);
    [codemapB, histB] = HEP_LBP(imgB, r, ftrType, crcType, fltType);
    
    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    LG = CreateImageLayers(imgG,disps,fltType);
    LB = CreateImageLayers(imgB,disps,fltType);
    imgR = repmat(imgR, [1 1 size(disps,1)]);
    imgG = repmat(imgG, [1 1 size(disps,1)]);
    
    %Define the weight mask
    mask = 2.^(0:size(disps,1)-1);                 
    codes = 0:sum(ones(size(mask)).*mask);     
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(img,1) size(img,2) 1]);
    
    %Compute the inter-channel codemaps R/G, R/B and G/B   
    comparisonsRG = (imgR <= LG);
    codemapRG = sum(comparisonsRG.*mask,3);
    comparisonsRB = (imgR <= LB);
    codemapRB = sum(comparisonsRB.*mask,3);
    comparisonsGB = (imgG <= LB);
    codemapGB = sum(comparisonsGB.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(disps,1);
    nColours = 2;
    nType = 'PERI';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN')
        pType = 'all';
        [codemapRG, codes] = PostprocessCodes(codemapRG, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapRB, codes] = PostprocessCodes(codemapRB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
        [codemapGB, codes] = PostprocessCodes(codemapGB, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        [codemapRG, codes] = PostprocessCodes(codemapRG, nBeads, nColours,...
            'CN', 'none', nType, pType);
        [codemapRB, codes] = PostprocessCodes(codemapRB, nBeads, nColours,...
            'CN', 'none', nType, pType);
        [codemapGB, codes] = PostprocessCodes(codemapGB, nBeads, nColours,...
            'CN', 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
    
    %Compute and concatenate the normalised histogram
    histRG = hist(codemapRG(:), codes)/numel(codemapRG);
    histRB = hist(codemapRB(:), codes)/numel(codemapRB);
    histGB = hist(codemapGB(:), codes)/numel(codemapGB);
    
    %Put everything together
    codemap = cat(3, codemapR,  codemapG,  codemapB,...
                     codemapRG, codemapRB, codemapGB);
    h = [histR, histG, histB, histRG, histRB, histGB];
    
    %Normalise hitogram to sum one
    h = h/6;
end