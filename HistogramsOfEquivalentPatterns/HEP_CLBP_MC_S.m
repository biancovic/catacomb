function [h] = HEP_CLBP_MC_S(img, r, ftrType, crcType, fltType)
%Concatenation of Completed Local Binary Patterns 'MC' and Locall Binary
%Patterns
%**********
%Synopsis:  HEP_CLBP_MC_S(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which HEP_CLBP_MC is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For deatils see Ref. [2].
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%h:         The normalised histogram of HEP_CLBP_MC_S codes (sums one)
%**********
%
%   Sample usage
%   ------------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      h = HEP_CLBP_MC_S(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      h = HEP_CLBP_MC_S(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]        Guo, Z., Zhang, L., Zhang, D.
%           A completed modeling of local binary pattern operator for 
%           texture classification (2010) IEEE Transactions on Image 
%           Processing, 19 (6), art. no. 5427137, pp. 1657-1663.
%[2]        Bianconi, F., Gonz�lez, E.
%           Counting local n-ary patterns
%           (2019) Pattern Recognition Letters, 117, pp. 24-29. 


    [rows cols] = size(img);

    %Compute CLBP_MC 
    [~,h_CLBP_MC,~] = HEP_CLBP_MC(img, r, ftrType, crcType, fltType);
    
    %Compute LBP 
    [~,h_LBP,~] = HEP_LBP(img, r, ftrType, crcType, fltType);

    %Concatenate and normalise the histogram
    h = [h_CLBP_MC, h_LBP];
    h = h/sum(h);

end