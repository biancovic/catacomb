function [codemap,h,codes] = HEP_CLBP_M(img, r, ftrType, crcType, fltType)
%An implementation of Completed Local Binary Patterns 'M' based on digital circles
%**********
%Synopsis:  HEP_CLBP_M(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radius of the circle. An integer representing the resolution 
%           (in pixels) at which CLBP_M is computed.
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For details see Ref. [2].
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemap:   The map of CLBP_M codes (codeword image)
%h:         The normalised histogram of CLBP_M codes (sums one)
%codes:     The possible codes
%**********
%
%   Sample usage
%   ------------
%      %Example 1
%      %Resolution = 2 pixels, directional features, circular repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_CLBP_M(img, 2, 'dir', 'Andres', 'circular');
%
%      %Example 2
%      %Resolution = 1 pixels, rotation-invariant features, symmetric repetition across
%      %the image borders, Andres' circles
%      img = imread('rice.png');
%      [codemap, h] = HEP_CLBP_M(img, 2, 'CN', 'Andres', 'symmetric');
%
%----------
%References
%[1]   Guo, Z., Zhang, L., Zhang, D.
%      A completed modeling of local binary pattern operator for 
%      texture classification (2010) IEEE Transactions on Image 
%      Processing, 19 (6), art. no. 5427137, pp. 1657-1663.
%[2]   Bianconi, F., Gonz�lez, E.
%      Counting local n-ary patterns
%      (2019) Pattern Recognition Letters, 117, pp. 24-29. 

    %Generate the neighbourhood
    disps = DrawCircle(r, crcType);
    
    %Layerize the input image
    L = CreateImageLayers(img,disps,fltType);
    
    %Compute the absolute differences
    img = repmat(img, [1 1 size(L,3)]);
    absdiffs = abs(img - L);
    
    %Compute the threshold
    T = mean(absdiffs(:));
    
    %Compute the CLBP_M codes
    mask = 2.^(0:size(L,3)-1);                  %Mask of weights
    codes = 0:sum(ones(size(mask)).*mask);      %Possible codes
    mask = reshape(mask, [1 1 numel(mask)]);
    mask = repmat(mask, [size(L,1) size(L,2) 1]);
    comparisons = (absdiffs <= T);
    codemap = sum(comparisons.*mask,3);
    
    %Postprocess the codes if required
    nBeads = size(L,3);
    nColours = 2;
    nType = 'PERI';
    if strcmp(ftrType,'dir')
        %Do nothing
    elseif strcmp(ftrType,'CN') || strcmp(ftrType,'DN')
        pType = 'all';
        [codemap, codes] = PostprocessCodes(codemap, nBeads, nColours,...
            ftrType, 'none', nType, pType);
    elseif strcmp(ftrType,'CNu2')
        pType = 'uni';
        [codemap, codes] = PostprocessCodes(codemap, nBeads, nColours,...
            'CN', 'none', nType, pType);
    else
        error('Feature type unsupported');
    end
    
    %Compute the normalised histogram
    h = hist(codemap(:), codes)/numel(codemap);

end