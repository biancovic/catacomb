function [codemaps, h] = HEP_ELBP(img, r, ftrType, crcType, fltType)
%Extended Local Binary Patterns
%**********
%Synopsis:  HEP_ELBP(img, r, ftrType, crcType, fltType)
%**********
%INPUT
%img:       The input image. An n x m matrix of double or integers.
%r:         The radii of the concentric circles used to compute the 
%           features. A 1 x c array of integers sorted in ascending order. 
%ftrType:   The feature type. Can be:
%               -> 'dir'    (directional, non rotation-invariant features)
%               -> 'CN'     (rotation-invariant features, i.e.: cyclic group CN)
%               -> 'DN'     (rotation and reflection-invariant features, 
%                            i.e.: dihedral group DN)
%               -> 'CNu2'   (same as 'CN', but uniform patterns. Note that 
%                            these are also reflection-invariant patterns [2])
%           For details see Ref. [2]
%crcType:   The type of digital circle - for possible values see DrawCircle()
%fltType:   How filtering is performed across the image borders - for
%           possible values see CreateImageLayers()
%**********
%OUTPUT
%codemaps:  The map of LBP codes (stack of codeword images)
%h:         The normalised histogram of ELBP codes (sums one)
%**********
%
%   Sample usage
%   ------------
%      %Example 1
%      %Compute ELBP on a neighbpurhood composed of three concentric, eight-point 
%      %digital circles of radius 1px, 2px and 3px. Directional features, 
%      %circular repetition across
%      img = imread('rice.png');
%      [codemap, h] = HEP_ELBP(img, [1 2 3], 'dir', 'eight-point-circle', 'circular');
%
%      %Example 2
%      %Compute ELBP on a neighbpurhood composed of four concentric, eight-point 
%      %digital circles of radius 2px, 3px, 5px and 6px. Rotation-invariant features, 
%      %circular repetition across
%      img = imread('rice.png');
%      [codemap, h] = HEP_ELBP(img, [2 3 5 6], 'CN', 'Andres', 'circular');
%
%----------
%References
%[1]    L. Liu, L. Zhao, Y. long G. Kuang and P. Fieguth; Extended local
%       binary patterns for texture classification, Image and Vision Computing
%       30 (2012):86-89 
%[2]    Bianconi, F., Gonz�lez, E.
%       Counting local n-ary patterns
%       (2019) Pattern Recognition Letters, 117, pp. 24-29. 


    %Compute LBP features
    LBPcodemaps = [];
    LBPhists = [];
    for i = 1:numel(r)
        [codemap, h] = HEP_LBP(img, r(i), ftrType, crcType, fltType);
        LBPcodemaps = cat(3, LBPcodemaps, codemap);
        LBPhists = [LBPhists, h];
    end
    
    %Compute BGC features
    BGCcodemaps = [];
    BGChists = [];
    for i = 1:numel(r)
        [codemap, h] = HEP_BGC(img, r(i), ftrType, crcType, fltType);
        BGCcodemaps = cat(3, BGCcodemaps, codemap);
        BGChists = [BGChists, h];
    end
    
    %Computed RDLBP features
    pairs = nchoosek(r, 2);
    RDLBPcodemaps = [];
    RDLBPhists = [];
    for i = 1:size(pairs, 1)
        [codemap, h] = HEP_RDLBP(img, pairs(i,:), ftrType, crcType, fltType);
        RDLBPcodemaps = cat(3, RDLBPcodemaps, codemap);
        RDLBPhists = [RDLBPhists, h];
    end

    %Put it all together
    codemaps = cat(3, LBPcodemaps, BGCcodemaps, RDLBPcodemaps);
    h = [LBPhists, BGChists, RDLBPhists];
end