function [OPP] = RGB_to_OPP(I, nL)
%RGB to Opponent colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%
%OPP:       The image in the Opponent colour space. The number of levels is the
%           same as the input image (i.e.: nL).
%
%   Sample usage
%   --------
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      OPP = RGB_to_OPP(RGB, 2^(imgInfo.BitDepth/3));
%

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    minRG = -1.0;
    maxRG = 1.0;
    minYeB = -2.0;
    maxYeB = 2.0;
    minWhBl = 0.0;
    maxWhBl = 3.0;
    
    %Squeeze the image
    R = reshape(I(:,:,1), [rows*cols 1]);
    G = reshape(I(:,:,2), [rows*cols 1]);
    B = reshape(I(:,:,3), [rows*cols 1]);
    
    %Apply the transform
    RG = R - G;
    YeB = -R - G + 2*B;
    WhBl =  R + G + B;
    
    %Normalize the result in the [0,1] interval
    RG = (RG - minRG)/(maxRG - minRG);
    YeB = (YeB - minYeB)/(maxYeB - minYeB);
    WhBl = (WhBl - minWhBl)/(maxWhBl - minWhBl);
    
    %Regenerate the image
    OPP = reshape(RG, [rows cols]);
    OPP = cat(3, OPP, reshape(YeB, [rows cols]));
    OPP = cat(3, OPP, reshape(WhBl, [rows cols]));
    
    %Convert back to int
    OPP = round((nL - 1)*OPP);
    switch nL
        case 2^8
            OPP = uint8(OPP);
        case 2^16
            OPP = uint16(OPP);
        otherwise
            error('Bit depth unsupported');
    end

end