function [XYZ,XYZN,XYZGamut] = RGB_to_XYZ(I, nL, M, T)
%RGB to XYZ colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%M,T        The coefficients of the linear transform from RGB to XYZ
%           coordinates previously determined through colour calibration - 
%           see ColourCalibration(). M is a 3 x 3 matrix, T a 3 x 1 array.
%
%XYZN:      The image in the XYZ colour space. Values are normalised and 
%           discretised in the [0,nL-1] interval.
%XYZ:       The image in the XYZ colour space. Values are in double
%           format and not normalised.
%XYZGamut:  The gamut's boundary (images in the XYZ space of the vertices
%           of the RGB cube).
%
%   Sample usage
%   --------
%      Let M, T are available from colour calibration
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      XYZ = RGB_to_XYZ(RGB, 2^(imgInfo.BitDepth/3), M, T);

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    %Determine the extreme values (gamut) of X, Y and Z
    RGBGamut = [0, 0, 0, 0, 1, 1, 1, 1;...
                0, 1, 0, 1, 0, 1, 0, 1;...
                0, 0, 1, 1, 0, 0, 1, 1];
    XYZGamut = M*RGBGamut + repmat(T, [1 8]);        
    
    maxX = max(XYZGamut(1,:));
    minX = min(XYZGamut(1,:));
    maxY = max(XYZGamut(2,:));
    minY = min(XYZGamut(2,:));
    maxZ = max(XYZGamut(3,:));
    minZ = min(XYZGamut(3,:));
    
    %Squeeze the image
    R = reshape(I(:,:,1), [1 rows*cols]);
    G = reshape(I(:,:,2), [1 rows*cols]);
    B = reshape(I(:,:,3), [1 rows*cols]);
    
    %Apply the transform
    XYZ = M*[R;G;B] + repmat(T, [1 rows*cols]);
        
    %Normalize the result in the [0,1] interval
    XYZN(1,:) = (XYZ(1,:) - minX)/(maxX - minX);
    XYZN(2,:) = (XYZ(2,:) - minY)/(maxY - minY);
    XYZN(3,:) = (XYZ(3,:) - minZ)/(maxZ - minZ);
    
    %Convert back to int
    XYZN = round((nL - 1)*XYZN);

    switch nL
        case 2^8
            XYZN = uint8(XYZN);
        case 2^16
            XYZN = uint16(XYZN);
        otherwise
            error('Bit depth unsupported');
    end
    
    %Regenerate the image
    X = reshape(XYZ(1,:), [rows cols]);
    Y = reshape(XYZ(2,:), [rows cols]);
    Z = reshape(XYZ(3,:), [rows cols]);

    XYZ = X;
    XYZ = cat(3, XYZ, Y);
    XYZ = cat(3, XYZ, Z);
    
    XN = reshape(XYZN(1,:), [rows cols]);
    YN = reshape(XYZN(2,:), [rows cols]);
    ZN = reshape(XYZN(3,:), [rows cols]);

    XYZN = XN;
    XYZN = cat(3, XYZN, YN);
    XYZN = cat(3, XYZN, ZN);

end