function [OHTA] = RGB_to_OHTA(I, nL)
%RGB to OHTA colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%
%OHTA:      The image in the OHTA colour space. The number of levels is the
%           same as the input image (i.e.: nL).
%
%   Sample usage
%   --------
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      OHTA = RGB_to_OHTA(RGB, 2^(imgInfo.BitDepth/3));
%

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    minI1 = 0.0;
    maxI1 = 1.0;
    minI2 = -0.5;
    maxI2 = 0.5;
    minI3 = -0.5;
    maxI3 = 0.5;
    
    %Squeeze the image
    R = reshape(I(:,:,1), [rows*cols 1]);
    G = reshape(I(:,:,2), [rows*cols 1]);
    B = reshape(I(:,:,3), [rows*cols 1]);
    
    %Apply the transform
    I1 =  (R + G + B)/3;
    I2 = (R - B)/2;
    I3 =  (- R + 2*G - B)/4;
    
    %Normalize the result in the [0,1] interval
    I1 = (I1 - minI1)/(maxI1 - minI1);
    I2 = (I2 - minI2)/(maxI2 - minI2);
    I3 = (I3 - minI3)/(maxI3 - minI3);
    
    %Regenerate the image
    OHTA = reshape(I1, [rows cols]);
    OHTA = cat(3, OHTA, reshape(I2, [rows cols]));
    OHTA = cat(3, OHTA, reshape(I3, [rows cols]));
    
    %Convert back to int
    OHTA = round((nL - 1)*OHTA);
    switch nL
        case 2^8
            OHTA = uint8(OHTA);
        case 2^16
            OHTA = uint16(OHTA);
        otherwise
            error('Bit depth unsupported');
    end

end