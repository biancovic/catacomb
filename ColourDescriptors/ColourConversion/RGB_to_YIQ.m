function [YIQ] = RGB_to_YIQ(I, nL)
%RGB to YIQ colour conversion
%
%INPUT
%I:        An RGB colour Iage
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%
%YIQ:       The Iage in the YIQ colour space. The number of levels is the
%           same as the input Iage (i.e.: nL).
%
%   Sample usage
%   --------
%      IgInfo = Ifinfo('board.tif','tiff');
%      RGB = Iread('board.tif');
%      YIQ = RGB_to_YIQ(RGB, 2^(IgInfo.BitDepth/3));
%

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    minY = 0.0;
    maxY = 1.0;
    minI = -0.596;
    maxI = 0.596;
    minQ = -0.523;
    maxQ = 0.523;
    
    %Squeeze the Iage
    R = reshape(I(:,:,1), [rows*cols 1]);
    G = reshape(I(:,:,2), [rows*cols 1]);
    B = reshape(I(:,:,3), [rows*cols 1]);
    
    %Apply the transform
    Y =  0.299*R + 0.587*G + 0.114*B;
    I = 0.596*R - 0.274*G - 0.322*B;
    Q =  0.211*R - 0.523*G + 0.312*B;
    
    %Normalize the result in the [0,1] interval
    Y = (Y - minY)/(maxY - minY);
    I = (I - minI)/(maxI - minI);
    Q = (Q - minQ)/(maxQ - minQ);
    
    %Regenerate the Iage
    YIQ = reshape(Y, [rows cols]);
    YIQ = cat(3, YIQ, reshape(I, [rows cols]));
    YIQ = cat(3, YIQ, reshape(Q, [rows cols]));
    
    %Convert back to int
    YIQ = round((nL - 1)*YIQ);
    switch nL
        case 2^8
            YIQ = uint8(YIQ);
        case 2^16
            YIQ = uint16(YIQ);
        otherwise
            error('Bit depth unsupported');
    end

end