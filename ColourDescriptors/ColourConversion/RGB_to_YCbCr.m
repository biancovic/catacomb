function [YCbCr] = RGB_to_YCbCr(I, nL)
%RGB to YCbCr colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%
%YCbCr:       The image in the YCbCr colour space. The number of levels is the
%           same as the input image (i.e.: nL).
%
%   Sample usage
%   --------
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      YCbCr = RGB_to_YCbCr(RGB, 2^(imgInfo.BitDepth/3));
%

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    minY = 0.0;
    maxY = 1.0;
    minCb = -0.5;
    maxCb = 0.5;
    minCr = -0.499;
    maxCr = 0.5;
    
    %Squeeze the image
    R = reshape(I(:,:,1), [rows*cols 1]);
    G = reshape(I(:,:,2), [rows*cols 1]);
    B = reshape(I(:,:,3), [rows*cols 1]);
    
    %Apply the transform
    Y =  0.299*R + 0.587*G + 0.114*B;
    Cb = -0.169*R - 0.331*G + 0.5*B;
    Cr =  0.5*R - 0.418*G - 0.081*B;
    
    %Normalize the result in the [0,1] interval
    Y = (Y - minY)/(maxY - minY);
    Cb = (Cb - minCb)/(maxCb - minCb);
    Cr = (Cr - minCr)/(maxCr - minCr);
    
    %Regenerate the image
    YCbCr = reshape(Y, [rows cols]);
    YCbCr = cat(3, YCbCr, reshape(Cb, [rows cols]));
    YCbCr = cat(3, YCbCr, reshape(Cr, [rows cols]));
    
    %Convert back to int
    YCbCr = round((nL - 1)*YCbCr);
    switch nL
        case 2^8
            YCbCr = uint8(YCbCr);
        case 2^16
            YCbCr = uint16(YCbCr);
        otherwise
            error('Bit depth unsupported');
    end

end