function [LuvN,Luv] = XYZ_to_Luv(I, nL, XYZGamut, illuminant)
%XYZ to Luv colour conversion
%
%INPUT
%I:             The image in the XYZ colour space. Values are in double
%               format and not normalised.
%nL:            Number of levels through which the output is is to be encoded
%               (typical value is 256)
%XYZGamut:      The gamut's boundary (images in the XYZ space of the vertices
%               of the RGB cube).
%illuminant:    The type of illuminant. Can be:
%                   'D65'
%
%OUTPUT
%LuvN:      The image in the LuvN colour space. Values are normalised and 
%           discretised in the [0,nL-1] interval.
%Luv:       The image in the Luv colour space. Values are in double
%           format and not normalised.
%
%   Sample usage
%   --------
%      Let M, T are available from colour calibration to convert from
%      device-dependent RGB to device-independent XYZ
%
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      nL = 2^(imgInfo.BitDepth/3); 
%      [XYZ,XYZN,XYZGamut] = RGB_to_XYZ(RGB, nL, M, T);
%      [LuvN,Luv] = XYZ_to_Luv(I, nL, XYZGamut, 'D65');

    %Set the tristimulus coordinates of the illuminant
    X0 = 0; Y0 = 0; Z0 = 0;
    switch illuminant
        case 'D65'
            X0 =95.047; Y0 =100.00; Z0=108.883;
        otherwise
            error('Reference white unsupported');
    end

    [rows cols channels] = size(I);

    %Determine the extreme values (gamut) of L, u and v
    [L,u,v] = XYZ_to_Luv_(XYZGamut(1,:),XYZGamut(2,:),XYZGamut(3,:),X0,Y0,Z0);
    
    maxL = max(L);
    minL = min(L);
    maxu = max(u);
    minu = min(u);
    maxv = max(v);
    minv = min(v);
    
    %Squeeze the image
    X = reshape(I(:,:,1), [1 rows*cols]);
    Y = reshape(I(:,:,2), [1 rows*cols]);
    Z = reshape(I(:,:,3), [1 rows*cols]);
    
    %Apply the transform
    [L,u,v] = XYZ_to_Luv_(X,Y,Z,X0,Y0,Z0);
        
    %Normalize the result in the [0,1] interval
    LuvN(1,:) = (L - minL)/(maxL - minL);
    LuvN(2,:) = (u - minu)/(maxu - minu);
    LuvN(3,:) = (v - minv)/(maxv - minv);
    
    %Convert back to int
    LuvN = round((nL - 1)*LuvN);

    switch nL
        case 2^8
            LuvN = uint8(LuvN);
        case 2^16
            LuvN = uint16(LuvN);
        otherwise
            error('Bit depth unsupported');
    end
    
    %Regenerate the image
    L = reshape(L, [rows cols]);
    u = reshape(u, [rows cols]);
    v = reshape(v, [rows cols]);

    Luv = L;
    Luv = cat(3, Luv, u);
    Luv = cat(3, Luv, v);
    
    LN = reshape(LuvN(1,:), [rows cols]);
    uN = reshape(LuvN(2,:), [rows cols]);
    vN = reshape(LuvN(3,:), [rows cols]);

    LuvN = LN;
    LuvN = cat(3, LuvN, uN);
    LuvN = cat(3, LuvN, vN);
end

function [L,u,v] = XYZ_to_Luv_(X,Y,Z,X0,Y0,Z0)
   
    L = 116*[f(Y./Y0)] - 16;
    u = 13*L.*[(4*X./(X+15*Y+3*Z)) - (4*X0./(X0+15*Y0+3*Z0))];
    v = 13*L.*[(9*Y./(X+15*Y+3*Z)) - (9*Y0./(X0+15*Y0+3*Z0))];
    
end

function [F] = f(x)
        if x > 0.008856
            F = x.^(1/3);
        else
            F = 7.787*x + (16/116);
        end
end