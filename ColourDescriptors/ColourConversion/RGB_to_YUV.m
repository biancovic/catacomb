function [YUV] = RGB_to_YUV(I, nL)
%RGB to YUV colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%
%YUV:       The image in the YUV colour space. The number of levels is the
%           same as the input image (i.e.: nL).
%
%   Sample usage
%   --------
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      YUV = RGB_to_YUV(RGB, 2^(imgInfo.BitDepth/3));
%

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    minY = 0.0;
    maxY = 1.0;
    minU = -0.436;
    maxU = 0.437;
    minV = -0.615;
    maxV = 0.615;
    
    %Squeeze the image
    R = reshape(I(:,:,1), [rows*cols 1]);
    G = reshape(I(:,:,2), [rows*cols 1]);
    B = reshape(I(:,:,3), [rows*cols 1]);
    
    %Apply the transform
    Y =  0.299*R + 0.587*G + 0.114*B;
    U = -0.147*R - 0.289*G + 0.437*B;
    V =  0.615*R - 0.515*G - 0.100*B;
    
    %Normalize the result in the [0,1] interval
    Y = (Y - minY)/(maxY - minY);
    U = (U - minU)/(maxU - minU);
    V = (V - minV)/(maxV - minV);
    
    %Regenerate the image
    YUV = reshape(Y, [rows cols]);
    YUV = cat(3, YUV, reshape(U, [rows cols]));
    YUV = cat(3, YUV, reshape(V, [rows cols]));
    
    %Convert back to int
    YUV = round((nL - 1)*YUV);
    switch nL
        case 2^8
            YUV = uint8(YUV);
        case 2^16
            YUV = uint16(YUV);
        otherwise
            error('Bit depth unsupported');
    end

end