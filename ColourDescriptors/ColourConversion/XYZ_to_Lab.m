function [LabN,Lab] = XYZ_to_Lab(I, nL, XYZGamut, illuminant)
%XYZ to Lab colour conversion
%
%INPUT
%I:             The image in the XYZ colour space. Values are in double
%               format and not normalised.
%nL:            Number of levels through which the output is is to be encoded
%               (typical value is 256)
%XYZGamut:      The gamut's boundary (images in the XYZ space of the vertices
%               of the RGB cube).
%illuminant:    The type of illuminant. Can be:
%                   'D65'
%
%OUTPUT
%LabN:      The image in the LabN colour space. Values are normalised and 
%           discretised in the [0,nL-1] interval.
%Lab:       The image in the Lab colour space. Values are in double
%           format and not normalised.
%
%   Sample usage
%   --------
%      Let M, T are available from colour calibration to convert from
%      device-dependent RGB to device-independent XYZ
%
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      nL = 2^(imgInfo.BitDepth/3); 
%      [XYZ,XYZN,XYZGamut] = RGB_to_XYZ(RGB, nL, M, T);
%      [LabN,Lab] = XYZ_to_Lab(I, nL, XYZGamut, 'D65');


    %Set the tristimulus coordinates of the illuminant
    X0 = 0; Y0 = 0; Z0 = 0;
    switch illuminant
        case 'D65'
            X0 =95.047; Y0 =100.00; Z0=108.883;
        otherwise
            error('Reference white unsupported');
    end

    [rows cols channels] = size(I);

    %Determine the extreme values (gamut) of L, a and b
    [L,a,b] = XYZ_to_Lab_(XYZGamut(1,:),XYZGamut(2,:),XYZGamut(3,:),X0,Y0,Z0);
    
    maxL = max(L);
    minL = min(L);
    maxa = max(a);
    mina = min(a);
    maxb = max(b);
    minb = min(b);
    
    %Squeeze the image
    X = reshape(I(:,:,1), [1 rows*cols]);
    Y = reshape(I(:,:,2), [1 rows*cols]);
    Z = reshape(I(:,:,3), [1 rows*cols]);
    
    %Apply the transform
    [L,a,b] = XYZ_to_Lab_(X,Y,Z,X0,Y0,Z0);
        
    %Normalize the result in the [0,1] interval
    LabN(1,:) = (L - minL)/(maxL - minL);
    LabN(2,:) = (a - mina)/(maxa - mina);
    LabN(3,:) = (b - minb)/(maxb - minb);
    
    %Convert back to int
    LabN = round((nL - 1)*LabN);

    switch nL
        case 2^8
            LabN = uint8(LabN);
        case 2^16
            LabN = uint16(LabN);
        otherwise
            error('Bit depth unsupported');
    end
    
    %Regenerate the image
    L = reshape(L, [rows cols]);
    a = reshape(a, [rows cols]);
    b = reshape(b, [rows cols]);

    Lab = L;
    Lab = cat(3, Lab, a);
    Lab = cat(3, Lab, b);
    
    LN = reshape(LabN(1,:), [rows cols]);
    aN = reshape(LabN(2,:), [rows cols]);
    bN = reshape(LabN(3,:), [rows cols]);

    LabN = LN;
    LabN = cat(3, LabN, aN);
    LabN = cat(3, LabN, bN);
end

function [L,a,b] = XYZ_to_Lab_(X,Y,Z,X0,Y0,Z0)
   
    L = 116*[f(Y./Y0)] - 16;
    a = 500*[f(X./X0) - f(Y./Y0)];
    b = 200*[f(Y./Y0) - f(Z./Z0)];
    
end

function [F] = f(x)
        if x > 0.008856
            F = x.^(1/3);
        else
            F = 7.787*x + (16/116);
        end
end



 