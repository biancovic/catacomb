function [HSV] = RGB_to_HSV(I, nL)
%RGB to HSV colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%
%HSV:       The image in the HSV colour space. The number of levels is the
%           same as the input image (i.e.: nL).
%
%   Sample usage
%   --------
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      HSV = RGB_to_HSV(RGB, 2^(imgInfo.BitDepth/3));
%

    %Normalize and convert to double
    I = double(I)/(nL - 1);
    
    %Tolerance for == 0
    tol = 1e-4;

    [rows cols channels] = size(I);

    minH = -60.0;
    maxH = 300.0;
    minS = 0.0;
    maxS = 1.0;
    minV = 0.0;
    maxV = 1.0;
    
    %Squeeze the image
    R = reshape(I(:,:,1), [rows*cols 1]);
    G = reshape(I(:,:,2), [rows*cols 1]);
    B = reshape(I(:,:,3), [rows*cols 1]);
    
    %Compute V
    V = max([R,G,B],[],2);
    
    %Compute S
    S = zeros(rows*cols,1);
    V_is_not_zero = abs(V) > tol;
    S(V_is_not_zero) = (V(V_is_not_zero) - min([R(V_is_not_zero),G(V_is_not_zero),B(V_is_not_zero)],[],2))./V(V_is_not_zero);
    
    %Compute H
    S_is_zero = (abs(S) < tol);
    R_is_max = (V == R) & (~S_is_zero);
    G_is_max = (V == G) & (~S_is_zero);
    B_is_max = (V == B) & (~S_is_zero);
    H(S_is_zero) = 0;
    H(R_is_max) = 60*(G(R_is_max) - B(R_is_max))./(S(R_is_max).*V(R_is_max));
    H(G_is_max) = 60*(2 + B(G_is_max) - R(G_is_max))./(S(G_is_max).*V(G_is_max));
    H(B_is_max) = 60*(4 + R(B_is_max) - G(B_is_max))./(S(B_is_max).*V(B_is_max));
    
    
    %Normalize the result in the [0,1] interval
    H = (H - minH)/(maxH - minH);
    S = (S - minS)/(maxS - minS);
    V = (V - minV)/(maxV - minV);
    
    %Regenerate the image
    HSV = reshape(H, [rows cols]);
    HSV = cat(3, HSV, reshape(S, [rows cols]));
    HSV = cat(3, HSV, reshape(V, [rows cols]));
    
    %Convert back to int
    HSV = round((nL - 1)*HSV);
    switch nL
        case 2^8
            HSV = uint8(HSV);
        case 2^16
            HSV = uint16(HSV);
        otherwise
            error('Bit depth unsupported');
    end

end