function [Lab,LabN] = RGB_to_Lab(I, nL, M, T)
%RGB to Lab colour conversion
%
%INPUT
%I:         An RGB colour image
%nL:        Number of levels through which each channel is encoded
%           (typical value is 256)
%M,T        The coefficients of the linear transform from RGB to Lab
%           coordinates previously determined through colour calibration - 
%           see ColourCalibration(). M is a 3 x 3 matrix, T a 3 x 1 array.
%
%LabN:      The image in the Lab colour space. Values are normalised and 
%           discretised in the [0,nL-1] interval.
%Lab:       The image in the Lab colour space. Values are in double
%           format and not normalised.
%
%   Sample usage
%   --------
%      Let M, T are available from colour calibration
%      imgInfo = imfinfo('board.tif','tiff');
%      RGB = imread('board.tif');
%      Lab = RGB_to_Lab(RGB, 2^(imgInfo.BitDepth/3), M, T);

    %Normalize and convert to double
    I = double(I)/(nL - 1);

    [rows cols channels] = size(I);

    %Determine the extreme values (gamut) of L, a and b
    RGBGamut = [0, 0, 0, 0, 1, 1, 1, 1;...
                0, 1, 0, 1, 0, 1, 0, 1;...
                0, 0, 1, 1, 0, 0, 1, 1];
    LabGamut = M*RGBGamut + repmat(T, [1 8]);        
    
    maxL = max(LabGamut(1,:));
    minL = min(LabGamut(1,:));
    maxa = max(LabGamut(2,:));
    mina = min(LabGamut(2,:));
    maxb = max(LabGamut(3,:));
    minb = min(LabGamut(3,:));
    
    %Squeeze the image
    R = reshape(I(:,:,1), [1 rows*cols]);
    G = reshape(I(:,:,2), [1 rows*cols]);
    B = reshape(I(:,:,3), [1 rows*cols]);
    
    %Apply the transform
    Lab = M*[R;G;B] + repmat(T, [1 rows*cols]);
        
    %Normalize the result in the [0,1] interval
    LabN(1,:) = (Lab(1,:) - minL)/(maxL - minL);
    LabN(2,:) = (Lab(2,:) - mina)/(maxa - mina);
    LabN(3,:) = (Lab(3,:) - minb)/(maxb - minb);
    
    %Convert back to int
    LabN = round((nL - 1)*LabN);

    switch nL
        case 2^8
            LabN = uint8(LabN);
        case 2^16
            LabN = uint16(LabN);
        otherwise
            error('Bit depth unsupported');
    end
    
    %Regenerate the image
    L = reshape(Lab(1,:), [rows cols]);
    a = reshape(Lab(2,:), [rows cols]);
    b = reshape(Lab(3,:), [rows cols]);

    Lab = L;
    Lab = cat(3, Lab, a);
    Lab = cat(3, Lab, b);
    
    LN = reshape(LabN(1,:), [rows cols]);
    aN = reshape(LabN(2,:), [rows cols]);
    bN = reshape(LabN(3,:), [rows cols]);

    LabN = LN;
    LabN = cat(3, LabN, a);
    LabN = cat(3, LabN, b);
end