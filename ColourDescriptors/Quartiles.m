function [featureVector] = Quartiles(I, G)
%Quartiles of each channel
%**********
%Synopsis:      f = Quintiles(I, G)
%**********
%INPUT:
%I:             A three-channel colour image
%G:             Number of levels through which each chnnel is encoded
%               (typical value is 256)
%**********
%OUTPUT:
%f:             The quartiles of each colour channel in the following order: 
%               [1st_qt_ch1, 2nd_qt_ch1, 3rd_qt_ch1,...
%               1st_qt_ch2, 2nd_qt_ch2, 3rd_qt_ch2,...]
%**********                  
%
%
%  Example
%  --------
%       imgInfo = imfinfo('rice.png','png');
%       G = 2^(imgInfo.BitDepth/3);
%       I = imread('rice.png');
%       f = Quartiles(I,G);
%
%References
%[1] M. Niskanen, O. Silv�n, and H. Kauppinen; Color and texture based
%    wood inspection with non-supervised clustering, inProc.of 12th Scandivanian
%    Conf.on Image Analysis, Bergen, Norway, pp.336�342(June 2001). 
    
    featureVector = [];
    
    [rows cols channels] = size(I);
    
    %Normalize input
    I = double(I)/(G - 1);
    
    for c = 1:channels
        featureVector = [featureVector prctile(reshape(I(:,:,c),1,rows*cols),[25 50 75])];
    end

end