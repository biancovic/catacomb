function [featureVector] = Mean_Hom(I,G)
%Mean value and homogeneity of each channel
%**********
%Synopsis:      f = Mean_Hom(I, G)
%**********
%INPUT:
%I:             A three-channel colour image
%G:             Number of levels through which each chnnel is encoded
%               (typical value is 256)
%**********
%OUTPUT:
%f:             The average values of each colour channel
%**********
%
%  Example
%  --------
%       imgInfo = imfinfo('rice.png','png');
%       G = 2^(imgInfo.BitDepth/3);
%       I=imread('rice.png');
%       f = Mean_Hom(I,G);
    
    [rows cols channels] = size(I);
    
    %Normalize input
    I = double(I)/(G-1);
    
    %Number of bins of the histogram to compute homogeneity
    nBins = G;
        
    %Compute histogram edges
    edges_R = 0:1/nBins:1;
    edges_G = 0:1/nBins:1;
    edges_B = 0:1/nBins:1;
    
    %Squeeze image
    I_ = zeros(rows*cols, channels);
    I_(:,1) = reshape(I(:,:,1),1,rows*cols);
    I_(:,2) = reshape(I(:,:,2),1,rows*cols);
    I_(:,3) = reshape(I(:,:,3),1,rows*cols);
    
    %Compute marginal histograms
    [RGB_hist_R] = histc(I_(:,1), edges_R);
    [RGB_hist_G] = histc(I_(:,2), edges_G);
    [RGB_hist_B] = histc(I_(:,3), edges_B);

    featureVector = [mean2(I(:,:,1)) mean2(I(:,:,2)) mean2(I(:,:,3))...
        Hom(RGB_hist_R, edges_R)...
        Hom(RGB_hist_G, edges_G)...
        Hom(RGB_hist_B, edges_B)];
end

function [H] = Hom(h, e)

    H = 0;

    %Normalize histogram
    h = h/sum(h);

    %Compute homogeneity
    n = numel(h);
    
    for i = 1:(n-1)
        H = H + h(i)/((e(i) + e(i+1))/2);
    end
    
    %Add contribute of the last bin
    H = H + h(n)/e(n);
        
    %Normalize
    max_Hom = 1/(e(1) + e(2))/2;
    H = H/max_Hom;

end