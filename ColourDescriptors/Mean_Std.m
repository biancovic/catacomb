function f = Mean_Std(I, G)
%Mean value and standard deviation of each channel
%**********
%Synopsis:      f = Mean_Std(I, G)
%**********
%INPUT:
%I:             A three-channel colour image
%G:             Number of levels through which each chnnel is encoded
%               (typical value is 256)
%**********
%OUTPUT:
%f:             The mean and standard deviation of each colour
%               channel in the following order: 
%               [mean_ch1, std_ch1, mean_ch2, std_ch2,...]
%**********                   
%
%
%  Example
%  --------
%       imgInfo = imfinfo('rice.png','png');
%       G = 2^(imgInfo.BitDepth/3);
%       I=imread('rice.png');
%       f = Mean_Std(I,G);
    
    f = [];
    
    [rows cols channels] = size(I);
    
    %Normalize input
    I = double(I)/(G - 1);
    
    for c = 1:channels
        f = [f, mean2(I(:,:,c)) std2(I(:,:,c))];
    end

end