function f = MarginalHistograms(I, G, nBins)
%Marginal colour histograms
%**********
%Synopsis:      f = MarginalHistograms(I, G, nBins)
%**********
%INPUT:
%I:             A three-channel colour image
%G:             Number of levels through which each channel is encoded
%               (typical value is 256)
%nBins:         Number of bins for each colour axis
%**********
%OUTPUT:
%f:             The marginal colour histograms in the following order
%               [1st_bin_ch1, 2nd_bin_ch1, ..., nth_bin_ch1,...
%               1st_bin_ch2, 2nd_bin_ch2, ..., nth_bin_ch2,...]
%**********
%
%  Example
%  --------
%       imgInfo = imfinfo('rice.png','png');
%       G = 2^(imgInfo.BitDepth/3);
%       I=imread('rice.png');
%       f = MarginalHistograms(I,G);
%
%References
%[1] M. Pietikäinen, S. Nieminen, E. Marszalec, and T. Ojala;  Accurate
%    color discrimination with classification based on features distributions,
%    in Proc. of 13th Int. Conf. on Pattern Recognition, Vol. 3, Vienna,
%    pp. 833–838 (August 1996).
    
    [rows cols channels] = size(I);
    
    %Normalize input
    I = double(I)/(G - 1);
    
    %Compute histogram edges
    edges_R = 0:1/(nBins(1)):1;
    edges_G = 0:1/(nBins(2)):1;
    edges_B = 0:1/(nBins(3)):1;
    
    %Squeeze image
    I_ = zeros(rows*cols, channels);
    I_(:,1) = reshape(I(:,:,1),1,rows*cols);
    I_(:,2) = reshape(I(:,:,2),1,rows*cols);
    I_(:,3) = reshape(I(:,:,3),1,rows*cols);
    
    %Compute marginal histograms
    [RGB_hist_R] = histc(I_(:,1), edges_R);
    [RGB_hist_G] = histc(I_(:,2), edges_G);
    [RGB_hist_B] = histc(I_(:,3), edges_B);
    
    RGB_hist_R(end - 1) = RGB_hist_R(end - 1) + RGB_hist_R(end);
    RGB_hist_G(end - 1) = RGB_hist_G(end - 1) + RGB_hist_G(end);
    RGB_hist_B(end - 1) = RGB_hist_B(end - 1) + RGB_hist_B(end);
    
    RGB_hist_R(end) = [];
    RGB_hist_G(end) = [];
    RGB_hist_B(end) = [];
    
    %Normalize histograms
    f = [RGB_hist_R'/(rows*cols) RGB_hist_G'/(rows*cols) RGB_hist_B'/(rows*cols)];
end