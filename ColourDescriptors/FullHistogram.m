function [h, codeMap] = FullHistogram(img, G, nBins)
%Joint 3D colour histogram
%**********
%Synopsis:      [h, codeMap] = FullHistogram(img, G, nBins)
%**********
%INPUT
%img:           A three-channel colour image
%G:             Number of levels through which each channel is encoded
%               (typical value is 256)
%nBins:         The number of bins for each colour channel
%**********
%OUTPUT:
%h:             The joint 3D colour histogram (array of nBins^3 elements)
%codeMap:       A map of colour codes indexed from 0 to nBins^3 - 1.
%**********
%
%   Sample usage
%   --------
%       G = 256;
%       img = imread('peppers.png');
%       h = FullHistogram(img, G, nBins);
%
%References
%[1]    Swain, M.J., Ballard, D.H. Color indexing
%       (1991) International Journal of Computer Vision, 7 (1)
    
    [rows cols channels] = size(img);
    
    %Normalize the input image
    img = single(img)/(G - 1);
    
    %Define the edges
    edges = single(linspace(0,1,nBins+1));
    lowerEdges = edges(1:end-1);
    lowerEdges = reshape(lowerEdges, [1 1 numel(lowerEdges)]);
    lowerEdges = repmat(lowerEdges, [rows cols 1]);
    upperEdges = edges(2:end);
    upperEdges = reshape(upperEdges, [1 1 numel(upperEdges)]);
    upperEdges = repmat(upperEdges, [rows cols 1]);

    
    %Find the colour level in each channel
    imgR = repmat(img(:,:,1), [1 1 size(lowerEdges,3)]);
    [row,col,levelR] = ind2sub(size(imgR), find((lowerEdges <= imgR) & (imgR <= upperEdges)));
    levelsMapR = single(zeros(rows,cols));
    levelsMapR(sub2ind([rows cols],row,col)) = levelR - 1;
    
    imgG = repmat(img(:,:,2), [1 1 size(lowerEdges,3)]);
    [row,col,levelG] = ind2sub(size(imgG), find((lowerEdges <= imgG) & (imgG <= upperEdges)));
    levelsMapG = single(zeros(rows,cols));
    levelsMapG(sub2ind([rows cols],row,col)) = levelG - 1;
    
    imgB = repmat(img(:,:,3), [1 1 size(lowerEdges,3)]);
    [row,col,levelB] = ind2sub(size(imgB), find((lowerEdges <= imgB) & (imgB <= upperEdges)));
    levelsMapB = single(zeros(rows,cols));
    levelsMapB(sub2ind([rows cols],row,col)) = levelB - 1;
    
    %Create the mask
    mask = nBins.^(0:2);

    %Compute the codemap
    codeMap = levelsMapR * mask(1) + levelsMapG * mask(2) + ...
              levelsMapB * mask(3);
    codeMap = uint16(codeMap);
    
    %Compute the histogram
    h = hist(codeMap(:), 0:nBins^3-1)/numel(codeMap);
    
end