function [f] = Mean_Moments(I,G)
%Mean value and moments from 3rd to 5th moment
%of each channel
%
%INPUT:
%I:                 A three-channel colour image
%G:                 Number of levels through which each channel is encoded
%                   (typical value is 256)
%
%OUTPUT:
%f:                 The mean, standard deviation and 3rd to 5th moments of each 
%                   colour channel in the following order: 
%                   [mean_ch1, std_ch1, mean_ch2, std_ch2,...
%                    2nd_mom_ch1, 3rd_mom_ch1, 4th_mom_ch1, 5th_mom_ch1,...
%                    2nd_mom_ch2, 3rd_mom_ch2, 4th_mom_ch1, 5th_mom_ch1,...]
%
%
%  Example
%  --------
%       imgInfo = imfinfo('rice.png','png');
%       G = 2^(imgInfo.BitDepth/3);
%       I=imread('rice.png');
%       f = Mean_Std_Moments(I,G);
    
    [rows cols channels] = size(I);
            
    %Compute the mean
    f = Mean_(I,G);
    
    %Normalize image
    I = double(I)/(G-1);
        
    %Pack channels
    R = reshape(I(:,:,1),1,rows*cols);
    G = reshape(I(:,:,2),1,rows*cols);
    B = reshape(I(:,:,3),1,rows*cols);
    
    %Compute moments - 1st channel
    f = [f moment(R,2)];
    f = [f moment(R,3)];
    f = [f moment(R,4)];
    f = [f moment(R,5)];
    
    %Compute moments - 2nd channel
    f = [f moment(R,2)];
    f = [f moment(G,3)];
    f = [f moment(G,4)];
    f = [f moment(G,5)];
    
    %Compute moments - 3rd channel
    f = [f moment(R,2)];
    f = [f moment(B,3)];
    f = [f moment(B,4)];
    f = [f moment(B,5)];

end