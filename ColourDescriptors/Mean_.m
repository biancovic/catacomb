function [f] = Mean_(I,G)
%Mean value of each channel
%**********
%Synopsis:      f = Mean_(I, G)
%**********
%INPUT:
%I:                 A three-channel colour image
%G:                 Number of levels through which each chnnel is encoded
%                   (typical value is 256)
%**********
%OUTPUT:
%f:                 The mean of each colour in the following order: 
%                   [mean_ch1, mean_ch2, mean_ch3]
%**********
%
%  Example
%  --------
%       imgInfo = imfinfo('rice.png','png');
%       G = 2^(imgInfo.BitDepth/3);
%       I = imread('rice.png');
%       f = Mean_(I,G);

    f = [];
    
    [rows cols channels] = size(I);
    
    %Normalize input
    I = double(I)/(G - 1);
    
    for c = 1:channels
        f = [f, mean2(I(:,:,c))];
    end

end