function F = ComputeCNNFeatures(img, net, layer, pooling, encoder, optArgs)
%Image features based on Convolutional Neural Networks
%**********
%Synopsis:         F = ComputeCNNFeatures(img, net, layer, pooling, encoder, optArgs)
%**********
%INPUT:
%img:              The input image  
%net:              The convolutional neural network 
%layer:            Number or name of the CNN's layer from which features are taken
%pooling:          The way the features are pooled. Can be:
%                        'FC' -> pooling is based on one of the net's
%                                fully-connected layers. In this case
%                                the layer chosen must be a fully-connected
%                                one.
%                        'EN' -> pooling is based on a pre-trained encoder
%                                which is passed as a parameter.
%encoder:          A pre-trained encoder -- see GeneratePoolingEncoderCNN(). 
%                  Required if pooling == 'EN'.
%optArgs:          Optional arguments for feature encoding -- see
%                  EncodeFeatures(). Only if pooling == 'EN'.
%**********
%OUTPUT:
%features:         The feature vector. If pooling == 'FC' the vector is 
%                  L2-normalized; otherwise the normalisation depends on optArgs. 
%
%**********
%Dependencies:
%MatConvNet: CNNs for Matlab 
%http://www.vlfeat.org/matconvnet/
%**********
%
%Sample usage
%   --------
%      %Example 1: Compute FC-CNN features
%      net = load('imagenet-vgg-m-128.mat');      %Load the CNN
%      img = imread('peppers.png');               %Load the image
%      layer = 20;                                %Select a FC layer
%      F = ComputeCNNFeatures(img, net, layer, 'FC');
%
%      %Example 2: Compute CNN features with BoW pooling
%      net = load('imagenet-vgg-m-128.mat');      %Load the CNN
%      img = imread('peppers.png');               %Load the image
%      layer = 13;                                %Select a conv. layer
%      %Create a BoW encoder
%      trainImages = './trainImages';             %Define the image repository
%      nClusters = 25;                            %Set the number of clusters   
%      BoWEncoder = GeneratePoolingEncoderCNN(...
%           trainImages, 'BoW', nClusters, net, layer);
%      F = ComputeCNNFeatures(img, net, layer, 'EN', BoWEncoder, []);
%
%      %Example 3: Compute CNN features with improved Fisher vector pooling
%      %First part is the same as Example 2  
%      IVFEncoder = GeneratePoolingEncoderCNN(...
%           trainImages, 'Fisher', nClusters, net, layer);
%      optArgs = {'Improved'};
%      F = ComputeCNNFeatures(img, net, layer, 'EN', IFVEncoder, optArgs);
%
%      %Example 4: Compute CNN features with VLAD and individually
%      L2-normalised subvectors.
%      %First part is the same as Examples 2 and 3  
%      VLADEncoder = GeneratePoolingEncoderCNN(...
%           trainImages, 'VLAD', nClusters, net, layer);
%      optArgs = {'NormalizeComponents'};
%      F = ComputeCNNFeatures(img, net, layer, 'EN', VLADEncoder, optArgs);
%
%
%
    %convert to single precision
    img = single(img);
        
    %Resize the image
    img = imresize(img, net.meta.normalization.imageSize(1:2));
        
    %Subtract the average image
    avgImg = net.meta.normalization.averageImage;
    if numel(avgImg) == 3
        avgImg = repmat(avgImg, [size(img,1) size(img,2) 1]);
    end
    img = img - avgImg;

    %Process the image through the net
    res = [];
    F = [];
    if isfield(net,'vars')
        %The net is a 'dag'
        net = dagnn.DagNN.loadobj(net);
        net.mode = 'test';
        net.vars(net.getVarIndex(layer)).precious = true;
        net.eval({'data', img});
        F = net.vars(net.getVarIndex(layer)).value;
    else
        %The net is a 'simple'
        res = vl_simplenn(net, img);
        F = res(layer).x;
    end
    
    switch pooling
        case 'FC'
            F = F(:);
            F = F/norm(F,2);
        case 'EN'
            F = reshape(shiftdim(F,2), [size(F,3) size(F,1)*size(F,2)]);
            F = EncodeFeatures(F, encoder, optArgs);
        otherwise('Pooling strategy unsupported')
    end
    
    F = F';
end

function [I, imgFile] = ConvertToGrey(img)

%Converts a colour image into grey-scale
%
%INPUT
%img    :   input image
%
%
%OUTPUT
%I          :   grey-scale output image
%imgFile    :   path to the temporary image
    
    I = rgb2gray(img);
    
    imgFile = './tempImg.bmp';
    imwrite(I, imgFile, 'bmp');
end

