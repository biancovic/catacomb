function encoder = GeneratePoolingEncoderCNN(trainImages, encType, nClusters,...
                                             net, layer)
%Generates pooling encoder for CNN-based features
%**********
%Synopsis:      encoder = GeneratePoolingEncoderCNN(trainImages, encType, nClusters, net, layer)
%**********
%INPUT
%trainImages:   Relative or absolute path to the folder containing the 
%               train images. These are used to generate the dictionary for
%               the encoding.
%encType:       The type of encoding. Can be:
%                   'BoW'       -> Bag-of-words encoding
%                   'Fisher'    -> Fisher vector encoding (FV)
%                   'VLAD'      -> Vector of Linearly Aggregated
%                                  Descriptors
%               See also [1,2] for details.
%nClusters:     Number of clusters used to encode the features (i.e. 
%               dimension of the dictionary)
%net:           The CNN used to create the encoder
%layer:         The CNN's layer used to create the encoder. This is usually the 
%               last convolutional, non fully-connected layer. Can be
%               either the index or the name of the layer
%**********
%OUTPUT
%encoder:       The structure containing the data for the encoding. 
%               See GenerateFeatureEncoder()
%**********
%References
%[1]    Cimpoi, M., Maji, S., Kokkinos, I., Vedaldi, A.
%       Deep Filter Banks for Texture Recognition, Description, and Segmentation
%       (2016) International Journal of Computer Vision, 118 (1), pp. 65-94.
%[2]    J�gou, H., Perronnin, F., Douze, M., S�nchez, J., P�rez, P., Schmid, C.
%       Aggregating local image descriptors into compact codes
%       (2012) IEEE Transactions on Pattern Analysis and Machine Intelligence, 34 (9), 
%       art. no. 6104058, pp. 1704-1716.
%**********
%
%   Sample usage
%   --------
%      %Example: Create a Fisher vector encoder
%      myImages = './MyImages';                     %Define the image repository
%      nClusters = 20;                              %Set the number of clusters   
%      myNet = load('imagenet-vgg-m-128.mat');      %Load the CNN
%      layer = 13;                                  %Select the CNN's layer
%      encoder = GeneratePoolingEncoder(...
%           myImages, 'Fisher', nClusters, myNet, layer);

    trainData = single([]);

    %Get the train images
    images = GetFileNames(trainImages);
    
    %Get the average image from the net
    avgImg = net.meta.normalization.averageImage;
    imgSize = net.meta.normalization.imageSize(1:2);
    
    %Process the train images and collect the train data
    nImages = size(images,1);
    for i = 1:nImages
        img = [];
        imgFile = images(i,:);
        img = imread(imgFile);
        
        fprintf('Processing image %d of %d: %s\n', i, nImages, imgFile);
        
        %convert to single precision
        img = single(img);
        
        %Resize the image
        img = imresize(img, imgSize);

        %Subtract the average image  
        if numel(avgImg) == 3
            avgImg = repmat(avgImg, [size(img,1) size(img,2) 1]);
        end
        img = img - avgImg;
        
        %Process the image through the net
        X = [];
        if isfield(net,'vars')
            %The net is a 'dag'
            net_ = dagnn.DagNN.loadobj(net);
            net_.mode = 'test';
            net_.vars(net_.getVarIndex(layer)).precious = true;
            net_.eval({'data', img});
            X = net_.vars(net_.getVarIndex(layer)).value;
        else
            %The net is a 'simple'
            res = vl_simplenn(net, img);
            X = res(layer).x;
        end
        
        %Append to the train data
        trainDataThisNet = reshape(shiftdim(X,2), [size(X,3) size(X,1)*size(X,2)]);
        trainData = [trainDataThisNet, trainData];       
        
    end
    
    %Generate the encoder
    fprintf('Generating the encoder...\n');
    encoder = GenerateFeatureEncoder(trainData, encType, nClusters);
end

