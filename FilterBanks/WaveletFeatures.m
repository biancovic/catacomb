function [WSF, WCF] = WaveletFeatures(img, bitDepth, nLevels, wName, fType)
%Texture features based on wavelet decomposition. Features are the
%mean and standard deviation of the wavelet approximation and detail 
%coefficients [1]
%**********
%Synopsis:      [WSF, WCF] = WaveletFeatures(img, nLevels, wName, fType)
%**********
%INPUT:
%img:           The grey-scale input image
%bitDepth:      The number of bits into which the image is quantised
%nFreqs:        The number of decomposition levels
%wName:         The wavelet's name. See wavedec2() for possible values.               
%fType:         The type of features to be computed. Can be
%                   'dir'           ->  directional features
%                   'rot-inv-DFT'   ->  rotation-invariant features through
%                                       DFT normalisation
%**********
%OUTPUT:
%WSF:           Wavelet Statistical Features. The mean and standard
%               deviation of the approximation and detail coefficients.
%WCF:           Wavelet Co-occurrence Features. The following statistics
%               from each of the approximation and detail matrices:
%                   'Energy'
%                   'Entropy'
%                   'Contrast'
%                   'Correlation'
%                   'Homogeneity'
%********** 
%
%   Sample usage
%   --------
%      %Example 1: directional features, two-level decomposition, Haar
%      wavelet
%      img = imread('rice.png');
%      [WSF, WCF] = WaveletFeatures(img, 8, 2, 'haar', 'dir');
%
%      %Example 2: directional features, three-level decomposition,
%      Daubechies db1 wavelet, rotation-invariant features
%      img = imread('rice.png');
%      [WSF, WCF] = WaveletFeatures(img, 8, 2, 'db1', 'rot-inv-DFT');
%
%References
%[1] S. Arivazhagan and L. Ganesan, Texture classification using wavelet
%transform, Pattern Recognition Letters 24:1513-1521 (2013)

    WSF = [];
    
    [rows cols] = size(img);
    
    if rows ~= cols
        warning('Input image is not square; cropping the central part');
        if rows > cols
            dim = cols;
        else
            dim = rows;
        end
        img = CropImageFromCenter(img, dim, dim);
    end
    
    %Resize the image to the nearest power of 2
    img = imresize(img, [2^round(log2(rows)) 2^round(log2(cols))]);
    
    %Normalise the image
    img = double(img)/(2^bitDepth);
    
    %Compute the wavelet decomposition
    [c,s]=wavedec2(img,nLevels,wName);
    
    %Compute the wavelet features
    WSF_app_avg = zeros(nLevels, 1);
    WSF_app_std = zeros(nLevels, 1);
    WSF_det_avg = zeros(nLevels, 3);
    WSF_det_std = zeros(nLevels, 3);
    WCF_app = zeros(nLevels, 5);
    WCF_det = zeros(nLevels, 5, 3);
    for l = 1:nLevels
        %Statistical features (WSF)
        A = appcoef2(c,s,wName,l);
        WSF_app_avg(l) = (mean(A(:)));
        WSF_app_std(l) = (std(A(:)));
        [H, V, D] = detcoef2('all',c,s,l);
        WSF_det_avg(l,:) = [mean(H(:)), mean(D(:)), mean(V(:))];
        WSF_det_std(l,:) = [std(H(:)), std(D(:)), std(V(:))];
        
        %Co-occurrence features (WCF)
        WCF_app(l,:) = ComputeWCF(A);
        WCF_det(l,:,1) = ComputeWCF(H);
        WCF_det(l,:,2) = ComputeWCF(D);
        WCF_det(l,:,3) = ComputeWCF(V);
    end
        
    switch fType
        case 'dir'
            WSF = [WSF_app_avg(:)' WSF_app_std(:)'...
                   WSF_det_avg(:)' WSF_det_std(:)'];
            WCF = [WCF_app(:)' WCF_det(:)'];   
        case 'rot-inv-DFT'
            feats_avg_dft = abs(fft(WSF_det_avg,[],2));
            feats_std_dft = abs(fft(WSF_det_std,[],2));
            WSF = [WSF_app_avg(:)' WSF_app_std(:)'...
                   feats_avg_dft(:)' feats_std_dft(:)'];
            WCF_det_dft = abs(fft(WCF_det,[],3));
            WCF = [WCF_app(:)' WCF_det_dft(:)'];
        otherwise
            error('Type of features unsupported');
    end
end

function [wcf] = ComputeWCF(X)
%Wavelets Co-occurrence Features

    %Convert to uint8
    m = min(X(:));
    M = max(X(:));
    X = uint8(255 .* ((X - m) ./ (M - m)));
    
    %Compute the co-occurrence features
    wcf = GLCMFeatures(X, 1, 'HAR', false, 'AVG', 8);
end