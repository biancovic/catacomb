function feats = Sfeatures(img, bitDepth)
%Texture features based on Schmid's filter bank [1]. Features are the
%mean and standard deviation of the absolute value of the filters' response.
%The input image is normalised to zero-mean.
%**********
%Synopsis:      featureVector = Sfeatures(img, bitDepth)
%**********
%INPUT:
%img:           The grey-scale input image
%bitDepth:      The bit depth of the input image
%**********
%OUTPUT:
%feats:         The feature vector
%********** 
%
%   Sample usage
%   --------
%      %Example 1
%      img = imread('rice.png');
%      bitDepth = 8;
%      feats = Sfeatures(img, bitDepth);
%
%References
%[1] C. Schmid. Constructing models for content-based image retrieval. 
%    In Proceedings of the IEEE Conference on Computer Vision and 
%    Pattern Recognition, volume 2, pages 39-45, 2001
%
%Author:            Francesco Bianconi
%Last updated:      Sep. 2016
%
%NOTE:
%This function is a wrapper around makeSfilters() available at:
%http://www.robots.ox.ac.uk/~vgg/research/texclass/filters.html
%

    feats = [];

    %Generate the filter bank
    FB = makeSfilters();
    
    %Normalize the input image
    img = double(img)/(2^bitDepth);
    img = img - mean(img(:));
    
    %Apply the filters
    for i = 1:size(FB,3)
        response = abs(conv2(img, FB(:,:,i), 'same'));
        feats = [feats, mean(response(:)), std(response(:))];
    end
end