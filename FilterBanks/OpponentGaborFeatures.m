function feats = OpponentGaborFeatures(img, bitDepth, nFreqs, fRatio,...
                                       nOrnts, spBwidth, aRatio, cNorm, fType)
%Opponent Gabor features. 
%**********
%Synopsis:      feats = OpponentGaborFeatures(img, bitDepth, nFreqs, spBwidth, aRatio, fRatio, nOrnts, fType)
%**********
%INPUT:
%img:           A three-channel colour image
%bitDepth:      The bit depth of the input image
%nFreqs:        The number of frequencies
%nOrnts:        The number of orientations
%fRatio:        The frequency progression. E.g.: fRatio = 2 -> octave
%                   spacing; fRatio = sqrt(2) -> half-octave spacing
%spBwidth       The spatial-frequency bandwidth in units of octave. Typical
%               values are in the range [0.5,2.5]
%aRatio         The aspect ratio of the filter, i.e. the ratio between the 
%               semi-major and semi-minor axes of the Gaussian envelope.
%               Typical values are in the range [0.1,1.5].
%cNorm          Intra-channel contrast normalisation. If true the Gabor 
%               responses for one point in all frequencies and rotations 
%               for each channel are normalized to sum 1.               
%fType:         The type of features to be computed. Can be
%                   'dir'           ->  directional features
%                   'rot-inv-DFT'   ->  rotation-invariant features through
%                                       DFT normalisation
%**********
%OUTPUT:
%feats:         The feature vector. This is arranged as follows:
%                   3 x 2 nFreqs x nOrnts unichrome features;
%                   3 x 2 nchoosek(nFreqs,2) x Ornts opponent features
%********** 
%
%   Sample usage
%   --------
%      %Example 1: directional features, four frequencies, six
%      orientations, half-octave frequency spacing, spatial-frequency
%      bandwidth = 1 and aspect ratio = 0.5. Apply contrast normalisation
%      img = imread('peppers.png');
%      feats = OpponentGaborFeatures(img, 8, 4, sqrt(2), 6, 1, 0.5, true, 'dir');
%
%      %Example 2: rotation-invariant features, three frequencies, four
%      orientations, octave frequency spacing, spatial-frequency
%      bandwidth = 0.8 and aspect ratio = 1.2. Do not apply contrast
%      normalisation.
%      img = imread('peppers.png');
%      feats = OpponentGaborFeatures(img, 8, 3, 2, 4, 0.8, 1.2 , false, 'rot-inv-DFT');

%References
%[1]    A. Jain and G. Healey; "A multiscale representation including 
%       opponent color features for texture recognition"; IEEE Transactions
%       on Image Processing, 7(1):124-128
%
%NOTE: differently from [1], we compute both the mean and std. dev. of each
%unichrome and opponent response. 

    feats = [];
    
    %Normalize the input image
    img = double(img)/(2^bitDepth);
    meanR = mean(mean(img(:,:,1)));
    meanG = mean(mean(img(:,:,2)));
    meanB = mean(mean(img(:,:,3)));
    img(:,:,1) = img(:,:,1) - meanR;
    img(:,:,2) = img(:,:,2) - meanG;
    img(:,:,3) = img(:,:,3) - meanB;
    
    %Compute the wavelength (base wavelength = 2px)
    wavelength = [];
    count = 1;
    while numel(wavelength) < nFreqs
        wavelength = [wavelength, round(2*(fRatio^count))];
        wavelength = unique(wavelength);
        count = count + 1;
    end

    %Generate the filter bank
    FB = sg_createfilterbank(size(img),...
                             1./wavelength,... 
                             nFreqs,... 
                             nOrnts,...
                             'k',fRatio,...
                             'gamma',spBwidth,...
                             'eta',aRatio*spBwidth,...
                             'verbose',0,...
                             'user_freq',1);
    
    %Filter the image and keep the magnitude of the response
    rR = sg_filterwithbank(img(:,:,1), FB);
    rG = sg_filterwithbank(img(:,:,2), FB);
    rB = sg_filterwithbank(img(:,:,3), FB);
    magR = abs(sg_resp2samplematrix(rR,'normalize',0));
    magG = abs(sg_resp2samplematrix(rG,'normalize',0));
    magB = abs(sg_resp2samplematrix(rB,'normalize',0));
    
    %Perform contrast normalisation if required
    if cNorm
        magR = magR./repmat(sum(magR,3),[1 1 size(magR, 3)]);
        magG = magG./repmat(sum(magG,3),[1 1 size(magG, 3)]);
        magB = magB./repmat(sum(magB,3),[1 1 size(magB, 3)]);
    end
    
    %Map the indices
    idxMap = vec2mat(1:size(magR,3), nOrnts);
    
    %Compute the unichrome features
    unichromeFeatsR = [];
    unichromeFeatsG = [];
    unichromeFeatsB = [];
    for o = 1:nOrnts
        for f = 1:nFreqs
            response_R = magR(:,:,idxMap(f,o));
            response_G = magG(:,:,idxMap(f,o));
            response_B = magB(:,:,idxMap(f,o));
            
            unichromeFeatsR(f,o,1) = mean(response_R(:));
            unichromeFeatsR(f,o,2) = std(response_R(:));
            unichromeFeatsG(f,o,1) = mean(response_G(:));
            unichromeFeatsG(f,o,2) = std(response_G(:));
            unichromeFeatsB(f,o,1) = mean(response_B(:));
            unichromeFeatsB(f,o,2) = std(response_B(:));
        end
    end
    
    %Compute the opponent features
    opponentFeatsRG = [];
    opponentFeatsRB = [];
    opponentFeatsGB = [];
    for o = 1:nOrnts
        count = 1;
        for f1 = 1:nFreqs
            filtered_img_Rf1 = magR(:,:,idxMap(f1,o));
            filtered_img_Gf1 = magG(:,:,idxMap(f1,o));
            for f2 = (f1+1):nFreqs
                filtered_img_Gf2 = magG(:,:,idxMap(f2,o));
                filtered_img_Bf2 = magB(:,:,idxMap(f2,o));
                
                diffRG = filtered_img_Rf1/mean(filtered_img_Rf1(:)) - ...
                         filtered_img_Gf2/mean(filtered_img_Gf2(:));
                diffRB = filtered_img_Rf1/mean(filtered_img_Rf1(:)) - ...
                         filtered_img_Bf2/mean(filtered_img_Bf2(:));
                diffGB = filtered_img_Gf1/mean(filtered_img_Gf1(:)) - ...
                         filtered_img_Bf2/mean(filtered_img_Bf2(:));
                
                sqrDiffRG = sqrt(diffRG.^2);
                sqrDiffRB = sqrt(diffRB.^2);
                sqrDiffGB = sqrt(diffGB.^2);
                
                opponentFeatsRG(count,o,1) = mean(sqrDiffRG(:));
                opponentFeatsRG(count,o,2) = std(sqrDiffRG(:));
                opponentFeatsRB(count,o,1) = mean(sqrDiffRB(:));
                opponentFeatsRB(count,o,2) = std(sqrDiffRB(:));
                opponentFeatsGB(count,o,1) = mean(sqrDiffGB(:));
                opponentFeatsGB(count,o,2) = std(sqrDiffGB(:));
                
                count = count + 1;
            end
        end
    end
    
    switch fType
        case 'dir'
            %Do nothing
        case 'rot-inv-DFT'
            unichromeFeatsR = abs(fft(unichromeFeatsR,[],2));
            unichromeFeatsG = abs(fft(unichromeFeatsG,[],2));
            unichromeFeatsR = abs(fft(unichromeFeatsB,[],2));
            opponentFeatsRG = abs(fft(opponentFeatsRG,[],2));
            opponentFeatsRB = abs(fft(opponentFeatsRB,[],2));
            opponentFeatsGB = abs(fft(opponentFeatsGB,[],2));
        otherwise
            error('Type of features unsupported');
    end
    
    %Put everything together
    feats = [unichromeFeatsR(:)',...
             unichromeFeatsG(:)',...
             unichromeFeatsB(:)',...
             opponentFeatsRG(:)',...
             opponentFeatsRB(:)',...
             opponentFeatsGB(:)'];
end