function [response] = FilterImageMR8(img, filterBank)
%Image filtering through MR8 filters
%**********
%Synopsis:      FilterImageMR8(img, filterBank)
%**********
%INPUT
%img:           A grey-scale image 
%filterBank:    A bank of MR8 filters
%**********
%OUTPUT
%response:      The MR8 filter responses. This is a stack of eight layers,
%               each layer has the same dimension of the input image
%
%   Sample usage
%   --------
%      img = imread('peppers.png');
%      img = rgb2gray(img);
%      filters = makeRFSfilters();
%      filters = single(filters);
%      response = FilterImageMR8(img, filters);

    response = [];
    filteredImages = [];
    
    %Normalize the input image to zero mean and unit variance
    img = single(img);
    img = zscore(img);
    
    for f = 1:size(filterBank,3)
        filteredImages = cat(3, filteredImages, conv2(img, filterBank(:,:,f), 'same'));
    end
                
    %Normalise the response
    L = sqrt(sum(filteredImages.^2,3));
    K = log(1 + L/0.03)./L;
    K = repmat(K, [1 1 size(filteredImages, 3)]);
    filteredImages = K.*filteredImages;
                
    %Get the response of the isotropic filters
    response = cat(3, response, filteredImages(:,:,37),...
                                filteredImages(:,:,36));
                                        
    %Of the other filters retain the maximum response
    for f = 1:6:31
        maxResponse = max(filteredImages(:,:,f:f+5),[],3);
        response = cat(3, response, maxResponse);
    end 
end