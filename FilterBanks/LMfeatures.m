function feats = LMfeatures(img, bitDepth, fType)
%Texture features based on Leung and Malik's filter bank [1]. Features are the
%mean and standard deviation of the absolute value of the filters' response.
%The input image is normalised to zero-mean.
%**********
%Synopsis:      featureVector = LMfeatures(img, bitDepth, fType)
%**********
%INPUT:
%img:           The grey-scale input image
%bitDepth:      The bit depth of the input image
%fType:         The type of features to be computed. Can be
%                   'dir'       ->  directional features from the 36 anisotropic
%                                   filters
%                   'rot-inv'   ->  rotation-invariant features from the 12
%                                   isotropic filters
%                   'all'       ->  features from all the 48 filters
%**********
%OUTPUT:
%feats:         The feature vector
%********** 
%
%   Sample usage
%   --------
%      %Example 1: directional features
%      img = imread('rice.png');
%      bitDepth = 8;
%      feats = LMfeatures(img, bitDepth, 'dir');
%
%      %Example 2: rotation-invariant features
%      img = imread('rice.png');
%      bitDepth = 8;
%      feats = LMfeatures(img, bitDepth, 'dir');
%
%References
%[1] T. Leung and J. Malik. Representing and recognizing the visual 
%    appearance of materials using three-dimensional textons. 
%    International Journal of Computer Vision, 43(1):29-44, June 2001
%
%Author:            Francesco Bianconi
%First released:    Sep. 2016
%
%NOTE:
%This function is a wrapper around makeLMfilters() available at:
%http://www.robots.ox.ac.uk/~vgg/research/texclass/filters.html
%

    feats = [];

    %Generate the filter bank
    FB = makeLMfilters();
    
    %Select the required filters
    switch fType
        case 'all'
            %Do nothing
        case 'dir'
            FB = FB(:,:,1:36);
        case 'rot-inv'
            FB = FB(:,:,37:end);
        otherwise
            error('Type of features unsupported');
    end
    
    %Normalize the input image
    img = double(img)/(2^bitDepth);
    img = img - mean(img(:));
    
    %Apply the filters
    for i = 1:size(FB,3)
        response = abs(conv2(img, FB(:,:,i), 'same'));
        feats = [feats, mean(response(:)), std(response(:))];
    end
end