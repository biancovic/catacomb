function feats = GaborFeaturesSB(img, bitDepth, nFreqs, fRatio,...
                                 nOrnts, spBwidth, aRatio, cNorm, fType)
%Texture features based on Gabor filters. Features are the
%mean and standard deviation of the magnitude of the filters' responses.
%The input image is normalised to zero-mean. Implementation based on
%SimpleGabor toolbox.
%**********
%Synopsis:      feats = GaborFeaturesSB(img, bitDepth, nFreqs, spBwidth, aRatio, fRatio, nOrnts, fType)
%**********
%INPUT:
%img:           The grey-scale input image
%bitDepth:      The bit depth of the input image
%nFreqs:        The number of frequencies
%nOrnts:        The number of orientations
%fRatio:        The frequency progression. E.g.: fRatio = 2 -> octave
%                   spacing; fRatio = sqrt(2) -> half-octave spacing
%spBwidth       The spatial-frequency bandwidth in units of octave. Typical
%               values are in the range [0.5,2.5]
%aRatio         The aspect ratio of the filter, i.e. the ratio between the 
%               semi-major and semi-minor axes of the Gaussian envelope.
%               Typical values are in the range [0.1,1.5].
%cNorm          Contrast normalisation. If true the Gabor responses for one 
%               point in all frequencies and rotations are normalized 
%               to sum 1.               
%fType:         The type of features to be computed. Can be
%                   'dir'           ->  directional features
%                   'rot-inv-DFT'   ->  rotation-invariant features through
%                                       DFT normalisation
%**********
%OUTPUT:
%feats:         The feature vector
%********** 
%
%   Sample usage
%   --------
%      %Example 1: directional features, four frequencies, six
%      orientations, half-octave frequency spacing, spatial-frequency
%      bandwidth = 1 and aspect ratio = 0.5. Apply contrast normalisation
%      img = imread('rice.png');
%      feats = GaborFeaturesSB(img, 8, 4, sqrt(2), 6, 1, 0.5, true, 'dir');
%
%      %Example 2: rotation-invariant features, three frequencies, four
%      orientations, octave frequency spacing, spatial-frequency
%      bandwidth = 0.8 and aspect ratio = 1.2. Do not apply contrast
%      normalisation.
%      img = imread('rice.png');
%      feats = GaborFeaturesSB(img, 8, 3, 2, 4, 0.8, 1.2 , false, 'rot-inv-DFT');


    feats = [];
    
    %Normalize the input image
    img = double(img)/(2^bitDepth);
    img = img - mean(img(:));
    
    %Compute the wavelength (base wavelength = 2px)
    wavelength = [];
    counter = 1;
    while numel(wavelength) < nFreqs
        wavelength = [wavelength, round(2*(fRatio^counter))];
        wavelength = unique(wavelength);
        counter = counter + 1;
    end

    %Generate the filter bank
    FB = sg_createfilterbank(size(img),...
                             1./wavelength,... 
                             nFreqs,... 
                             nOrnts,...
                             'k',fRatio,...
                             'gamma',spBwidth,...
                             'eta',aRatio*spBwidth,...
                             'verbose',0,...
                             'user_freq',1);
    
    %Filter the image and keep the magnitude of the response
    r = sg_filterwithbank(img, FB);
    mag = abs(sg_resp2samplematrix(r,'normalize',0));
    
    %Perform contrast normalisation of required
    if cNorm
        mag = mag./repmat(sum(mag,3),[1 1 size(mag, 3)]);
    end
    
    %Map the indices
    idxMap = vec2mat(1:size(mag,3), nOrnts);
    
    %Compute the features and arrange them on a matrix where rows are
    %frequencies and columns orientations
    feats_avg = [];
    feats_std = [];
    for o = 1:nOrnts
        for f = 1:nFreqs
            filtered_img = mag(:,:,idxMap(f,o));
            feats_avg(f,o) = mean(filtered_img(:));
            feats_std(f,o) = std(filtered_img(:));
        end
    end
    
    switch fType
        case 'dir'
            feats = [feats_avg(:)' feats_std(:)'];
        case 'rot-inv-DFT'
            feats_avg_dft = abs(fft(feats_avg,[],2));
            feats_std_dft = abs(fft(feats_std,[],2));
            feats = [feats_avg_dft(:)' feats_std_dft(:)'];
        otherwise
            error('Type of features unsupported');
    end
end