function [response,feats] = MR8features(img, bitDepth)
%Texture features based on Varma and Zisserman's Maximum Response 8 (MR8) 
%filter bank [1]. Features are the mean and standard deviation of the 
%absolute value of the filters' response.
%The input image is normalised to zero-mean.
%**********
%Synopsis:      featureVector = LMfeatures(img, bitDepth, fType)
%**********
%INPUT:
%img:           The grey-scale input image
%bitDepth:      The bit depth of the input image
%**********
%OUTPUT:
%resp
%feats:         The feature vector
%********** 
%
%   Sample usage
%   --------
%      %Example 1
%      img = imread('rice.png');
%      bitDepth = 8;
%      feats = MR8features(img, bitDepth);
%
%References
%[1] M. Varma and A. Zisserman. Texture classification: are filter banks
%    necessary? 
%    Computer Vision and Pattern Recognition, 2003. Proceedings
%
%Author:            Francesco Bianconi
%First released:    Sep. 2016
%
%NOTE:
%This function is a wrapper around makeRFSfilters() available at:
%http://www.robots.ox.ac.uk/~vgg/research/texclass/filters.html
%

    feats = [];

    %Generate the filter bank
    FB = makeRFSfilters();
    
    %Normalize the input image
    img = double(img)/(2^bitDepth);
    img = img - mean(img(:));
    
    %Apply the filters
    MEAN_RES = [];
    STD_RES = []; 
    response = [];
    for i = 1:size(FB,3)
        response = cat(3, response, abs(conv2(img, FB(:,:,i), 'same')));
    end
    
    %Get the response of the isotropic filters
    isoResponse1 = response(:,:,37);
    isoResponse2 = response(:,:,38);
    feats = [mean(isoResponse1(:)), std(isoResponse1(:)), ...
             mean(isoResponse2(:)), std(isoResponse2(:))];
        
    %Of the other filters retain the maximum response
    for i = 1:6:31
        maxResponse = max(response(:,:,i:i+5),[],3);
        feats = [feats, mean(maxResponse(:)), std(maxResponse(:))];
    end    
end