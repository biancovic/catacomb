function encoder = GeneratePoolingEncoderVZ(trainImages, fType, encType, nClusters)
%Pooling encoder for VZ features
%**********
%Synopsis:      encoder = GeneratePoolingEncoderVZ(trainImages, fType, encType, nClusters)
%**********
%INPUT
%trainImages:   Relative or absolute path to the folder containing the 
%               train images.
%fType:         The type of filter to be used. Can be:
%                   'MR8'       -> MR8 filter bank
%encType:       The type of encoding. Can be:
%                   'BoW'       -> Bag-of-words encoding
%                   'Fisher'    -> Fisher vector encoding (FV)
%                   'VLAD'      -> Vector of Linearly Aggregated
%                                  Descriptors
%nClusters:     Number of clusters used to encode the features (i.e. 
%               dimension of the dictionary)
%**********
%OUTPUT
%encoder:       The structure containing the data for the encoding. 
%               See GenerateFeatureEncoder()
%**********
%
%   Sample usage
%   --------
%      %Example: Create a bag-of-words vector encoder over MR8 filter
%      %responses
%      trainImages = './MyImages';                  
%      nClusters = 16;                                                   
%      encoder = GeneratePoolingEncoderVZ(trainImages, 'MR8', 'BoW', nClusters);

%References
%[1]    M. Varma and A. Zissermann, "A statistical approach to texture 
%       classification from single images"; International Journal of
%       Computer Vision, 62(1-2):61-81  
    
    trainData = single([]);

    %Get the train images
    images = GetFileNames(trainImages);
    nTrainImages = size(images, 1);
    
    %Number of clusters for image
    nClustersPerImage = ones(1,nTrainImages);
    if nTrainImages > nClusters
        %Do nothing here -- see below
    else
        r = mod(nClusters, nTrainImages);
        n = (nClusters - r)/nTrainImages;
        nClustersPerImage = n*nClustersPerImage;
        nClustersPerImage(end) = nClustersPerImage(end) + r;
    end
    
    %Generate the filter bank
    filters = [];
    switch fType
        case 'MR8'
            filters = makeRFSfilters();
        otherwise
            error('Type of filter unsupported');
    end
    filters = single(filters);
    
    %Process the train images and collect the train data
    centers = [];
    means = [];
    covariances = [];
    priors = [];
    nImages = size(images,1);
    for i = 1:nImages
        img = [];
        imgFile = images(i,:);
        img = imread(imgFile);
        [rows cols ch] = size(img);
        
        %Convert to greyscale if required
        if ch > 1
            img = rgb2gray(img);
        end
        
        fprintf('Processing image %d of %d: %s\n', i, nImages, imgFile);
        
        %convert to single precision
        img = single(img);
        
        %Apply the filters
        response = [];
        switch fType
            case 'MR8'
                response = FilterImageMR8(img, filters);
            otherwise
                error('Type of filter unsupported');
        end
        
        X = reshape(permute(response, [3 2 1]),...
                            [size(response,3) size(response,1)*size(response,2)]);
        
        %Generate the encoders' data
        encoder = GenerateFeatureEncoder(X, encType, nClustersPerImage(i));   
        
        switch encType
            case 'BoW'
                centers_ = vl_kmeans(X, nClustersPerImage(i));
                centers = [centers, centers_];
            case 'Fisher'
                [centers_, covariances_, priors_] = ...
                    vl_gmm(X, nClustersPerImage(i));     
                centers = [centers, centers_];
                covariances = [covariances, covariances_];
                priors = [priors, priors_];
            case 'VLAD'
                centers_ = vl_kmeans(X, nClustersPerImage(i));
                centers = [centers, centers_];
            otherwise 
                error('Unsupported encoding');
        end
    end
    
    %If nTrainImages < nClusters use the cluster centers as train data
    encoder = [];
    if nTrainImages > nClusters
        encoder = GenerateFeatureEncoder(centers, encType, nClusters);
    else
        switch encType
            case 'BoW'            
                %Define the the data-to-cluster assignments through a kd-tree
                kdtree = vl_kdtreebuild(centers) ;      
                encoder = struct('type', 'BoW',...
                                 'nclusters', nClusters,...
                                 'centers', centers,...
                                 'kdtree', kdtree);
            case 'Fisher'    
                encoder = struct('type', 'Fisher',...
                                 'nclusters', nClusters,...
                                 'means', means,...
                                 'covariances', covariances,...
                                 'priors', priors);
            case 'VLAD'            
                kdtree = vl_kdtreebuild(centers) ;      
                encoder = struct('type', 'VLAD',...
                                 'nclusters', nClusters,...
                                 'centers', centers,...
                                 'kdtree', kdtree);
        otherwise 
            error('Unsupported encoding');
        end
    end
end

