function F = ComputeMR8Features(img, filters, encoder, optArgs)
%VZ-MR8 texture features
%**********
%Synopsis:         F = ComputeMR8Features(img, samplingStep, binSize, encoder, optArgs)
%**********
%INPUT:
%img:              The input image
%filters:          Bank of MR8 filters
%encoder:          A pre-trained encoder -- see GeneratePoolingEncoderMR8().
%optArgs:          Optional arguments for feature encoding -- see
%                  EncodeFeatures().
%**********
%OUTPUT:
%features:         The feature vector.  
%**********
%
%   Sample usage
%   --------
%
%      %Example 1: Compute MR8 features ncoded with BoW pooling
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      encType = 'BoW';                           %Type of encoder
%      filters = makeRFSfilters();;               %Generate the filter bank
%      BoWEncoder = GeneratePoolingEncoderVZ(trainImages, 'MR8', encType, ...
%                                            nClusters);
%      F = ComputeMR8Features(img, filters, BoWEncoder, []);
%
%      %Example 2: Compute dense MR8 features with improved Fisher vector pooling
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      encType = 'Fisher';                        %Type of encoder
%      filters = makeRFSfilters();;               %Generate the filter bank 
%      optArgs = {'Improved'};
%      IVFEncoder = GeneratePoolingEncoderVZ(trainImages, 'MR8', encType, ...
%                                            nClusters);
%      F = ComputeMR8Features(img, filters, IFVEncoder, optArgs);
%
%      %Example 3: Compute dense MR8 features pooled with VLAD and individually
%      L2-normalised subvectors.
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      encType = 'VLAD';                        %Type of encoder
%      filters = makeRFSfilters();;               %Generate the filter bank   
%      VLADEncoder = GeneratePoolingEncoderVZ(trainImages, 'MR8', encType, ...
%                                             nClusters);
%      optArgs = {'NormalizeComponents'};
%      F = ComputeMR8Features(img, filters, VLADEncoder, optArgs);

%References
%[1]    M. Varma and A. Zissermann, "A statistical approach to texture 
%       classification from single images"; International Journal of
%       Computer Vision, 62(1-2):61-81  

    [rows cols ch] = size(img);
    
    %Convert to grey-scale if required
    if ch >  1
        img = rgb2gray(img);
    end
    
    %Filter the image
    response = FilterImageMR8(img, filters);
        
    %Encode the filters' responses through the selected encoder
    X = reshape(permute(response, [3 2 1]),...
                        [size(response,3) size(response,1)*size(response,2)]);
    F = EncodeFeatures(single(X), encoder, optArgs);    
    F = F';
end



