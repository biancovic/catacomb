function F = HistogramsOfOrientedGradients(img, cellSize, nBins)
%Histogram of oriented gradients (HOG)
%**********
%Synopsis:         F = HistogramsOfOrientedGradients(img, cellSize, nBins)
%**********
%INPUT:
%img:              The input image
%cellSize:         The size of the HOG cells in pixels (must be an odd number)
%nBins:            Number of bins of the orientation histogram
%**********
%OUTPUT:
%features:         The feature vector.  
%**********
%
%   Sample usage
%   --------
%
%      %Example 1: 
%      img = imread('peppers.png');               %Load the image
%      img = rgb2gray(img);                       %Convert to grey-scale
%      cellSize = [3 3];
%      nBins = 256;
%      F = HistogramsOfOrientedGradients(img, cellSize, nBins);

    %Convert the input image to single
    img = single(img);

    %Create the generalised Sobel filters
    %X -> downwards
    %Y -> from left to right
    xCoord = (0:(cellSize-1))';
    xCoord = xCoord - floor(cellSize/2);
    xCoord = repmat(xCoord, [1 cellSize]);
    yCoord = rot90(xCoord);
    fx = single(xCoord ./ (xCoord.*xCoord + yCoord.*yCoord));
    fx(isnan(fx)) = 0;
    fy = single(yCoord ./ (xCoord.*xCoord + yCoord.*yCoord));
    fy(isnan(fy)) = 0;
    
    %Filter the image to estimate the gradient intensity
    hGrad = conv2(img, fx, 'valid');    %Top-down gradient intensity
    vGrad = conv2(img, fy, 'valid');    %Left-right gradient intensity
    
    %Estimate the gradient direction
    gradDir = atan2(vGrad, hGrad);
    
    %Remove the singularities (put them out of the range of the histogram)
    gradDir(isnan(gradDir)) = -2*pi;
    
    %Define the histogram edges
    histEdges = linspace(-pi, pi - 2*pi/nBins, nBins);
    
    %Compute the histogram of gradients
    F = hist(gradDir(:), histEdges);
    F = F/norm(F,1);

end



