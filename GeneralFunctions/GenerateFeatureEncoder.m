function encoder = GenerateFeatureEncoder(trainData, encType, nClusters)
%Generates a feature encoder from a set of train data. 
%This is a a wrapper around VLfeat's functions for Fisher vector and VLAD 
%encodings [1].
%**********
%Synopsis:      encoder = GenerateFeatureEncoder(features, encType, nClusters)
%**********
%INPUT
%trainData:     An N x M matrix where each column represents a train point.
%               N is the dimension of the feature space
%encType:       The type of encoding. Can be:
%                   'BoW'       -> Bag-of-words encoding
%                   'Fisher'    -> Fisher vector encoding (FV)
%                   'VLAD'      -> Vector of Linearly Aggregated
%                                  Descriptors
%nClusters:     Number of clusters used to encode the features (i.e. 
%               dimension of the dictionary)
%**********
%OUTPUT
%encoder:       A structure containing the data for the encoding. Depending
%               on the value of encType the fields are:
%                   type, nclusters             -- all
%                   means, covariances, priors  -- if encType == 'Fisher'
%                   centers, kdtree             -- if encType == 'BoW' or 'VLAD'
%********** 
%Dependencies
%VLFeat: http://www.vlfeat.org/
%**********
%
%   Sample usage
%   --------
%      %Example 1: BoW encoder
%      %Create 40 random 60-dimensional features
%      features = rand([60 40]); 
%      %Use these data to create a 6-dimensional encoder
%      nClusters = 6;
%      encoder = GenerateFeatureEncoder(features, 'BoW', nClusters);
%
%      %Example 2: Fisher vector encoder
%      %Create 20 random 100-dimensional features
%      features = rand([100 20]); 
%      %Use these data to create a 5-dimensional encoder
%      nClusters = 5;
%      encoder = GenerateFeatureEncoder(features, 'Fisher', nClusters);
%
%      %Example 3: VLAD encoder
%      %Create 30 random 80-dimensional features
%      features = rand([80 30]); 
%      %Use these data to create a 10-dimensional encoder
%      nClusters = 10;
%      encoder = GenerateFeatureEncoder(features, 'VLAD', nClusters);
%
%References
%[1]   A. Vedaldi and B. Fulkerson; "VLFeat: An Open and Portable Library
%      of Computer Vision Algorithms", 2008, http://www.vlfeat.org/

    encoder = struct();

    switch encType
        case 'BoW'
            %Use k-means to generate the dictionary
            centers = vl_kmeans(trainData, nClusters);
            
            %Define the the data-to-cluster assignments through a kd-tree
            kdtree = vl_kdtreebuild(centers) ;      
            encoder = struct('type', 'BoW',...
                             'nclusters', nClusters,...
                             'centers', centers,...
                             'kdtree', kdtree);
        case 'Fisher'
            %Use Gaussian mixture model to construct the dictionary
            [means, covariances, priors] = vl_gmm(trainData, nClusters);     
            encoder = struct('type', 'Fisher',...
                             'nclusters', nClusters,...
                             'means', means,...
                             'covariances', covariances,...
                             'priors', priors);
        case 'VLAD'
            %Use k-means to generate the dictionary
            centers = vl_kmeans(trainData, nClusters);
            
            %Define the the data-to-cluster assignments through a kd-tree
            kdtree = vl_kdtreebuild(centers) ;      
            encoder = struct('type', 'VLAD',...
                             'nclusters', nClusters,...
                             'centers', centers,...
                             'kdtree', kdtree);
        otherwise 
            error('Unsupported encoding');
    end
end

