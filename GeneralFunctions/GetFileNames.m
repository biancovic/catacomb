function [fileNames, classLabels] = GetFileNames(path)
%Returns the names of the files contained in a directory
%
%INPUT:
%path           : Path to the directory to explore
%OUTPUT:
%fileNames      : Names (full paths) of the files in the directory
%classLabels    : Class labels extracted from file names (class label
%                 is the part of the file name preceding '__')
%
%
%  Example
%  --------
%       [fileNames, classLabels] = GetFileNames('C:/myData');

% Version 0.1
% Authors: Francesco Bianconi

    classLabels = cell(0,0);

    %Read the list of files in the directory
    dirContents = dir(path);
    nFiles = size(dirContents,1);

    %Create the full names of the files
    count = 1;
    if nFiles <= 2
        fileNames = [];
    else
        for i = 1:nFiles
            record = fullfile(path, dirContents(i).name);
            recordLength = size(record,2);
            if isdir(record) ~= 1 
            
                %Store class label
                fullName = dirContents(i).name;
                label = fullName(1:findstr(fullName,'__')-1);
                classLabels = [classLabels; label];
            
                for j = 1:recordLength
                    fileNames(count,j) = record(j);
                end
                count = count + 1;
            end
        end
    end
    
    
end