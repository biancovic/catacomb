function [S] = CreateImageLayers3D(V, displacements, boundaryOptions)
%Distribute the input volume V onto a 4D stack of volumes each representing a 
%translated (and/or mirrored) copy of V 
%**********
%Synopsis:          CreateImageLayers3D(V, displacements, boundaryOptions)
%**********
%INPUT
%V:                 A 3D matrix 
%displacements:     A D x 3 vector respresenting a set of (x,y,z) displacements
%boundaryOptions:   How to cope with edge effects when the displacement
%                   gets across the image borders. Can be:
%                       'circular'
%                       'symmetric'                       
%                   - see also imfilter() - 
%**********
%OUTPUT
%L:                 An A x B X C x D matrix, where A, B and C are the dimensions
%                   of the input image
%**********
%
%  Sample usage
%  --------
%       V = rand([4 4 4]);                           
%       d = [0 1 0; 0 1 1];
%       bopt = 'circular';
%       S = CreateImageLayers(V, d, bopt);


    [rows cols, zeta] = size(V);
    D = size(displacements,1);
    
    switch boundaryOptions
        case 'circular'
            V = repmat(V, [3 3 3]);
        case 'symmetric'
            %To be implemented
            Vmiddle = [flipud(fliplr(V)) flipud(V) flipud(fliplr(V));...
                       fliplr(V) V fliplr(V);...
                       flipud(fliplr(V)) flipud(V) flipud(fliplr(V))];
            Vup     = flip(Vmiddle,3);
            Vdown   = Vup;
            V = cat(3, Vup, Vmiddle);
            V = cat(3, V, Vdown);
        otherwise 
            error('Unsupported boundary option');
    end

    %Origin
    x0 = rows + 1;
    y0 = cols + 1;
    z0 = zeta + 1;
    
    %Create the layers
    for d = 1:D
        xStart = x0 + displacements(d,1);
        yStart = y0 + displacements(d,2);
        zStart = z0 + displacements(d,3);
        xEnd = xStart + rows - 1;
        yEnd = yStart + cols - 1;
        zEnd = zStart + zeta - 1;
        S(:,:,:,d) = V(xStart:xEnd,yStart:yEnd,zStart:zEnd);
    end
        
end