function [L] = CreateImageLayers(I, displacements, boundaryOptions)
%Distribute the input image I onto a stack of layers each representing a 
%translated copy of I 
%**********
%Synopsis:          CreateImageLayers(I,displacements,boundaryOptions)
%**********
%INPUT
%I:                 A one-channel image 
%displacements:     A D x 2 vector respresenting a set of (x,y) displacements
%boundaryOptions:   How to cope with edge effects when the displacement
%                   gets across the image borders. Can be:
%                       'circular'
%                       'symmetric'                       
%                   - see also imfilter() - 
%**********
%OUTPUT
%L:                 An R x C x D matrix, where R and C are the dimensions
%                   of the input image. Each of the D layers contains a
%                   copy of the input image translated by the corresponding 
%                   vector 
%**********
%
%  Sample usage
%  --------
%       I=imread('rice.png');                           
%       d = [0 1; 1 1];
%       bopt = 'circular';
%       L = CreateImageLayers(I, d, bopt);

    [rows cols] = size(I);
    D = size(displacements,1);
    
    switch boundaryOptions
        case 'circular'
            I = [I I I; I I I; I I I];
        case 'symmetric'
            I = [flipud(fliplr(I)) flipud(I) flipud(fliplr(I));...
                 fliplr(I) I fliplr(I);...
                 flipud(fliplr(I)) flipud(I) flipud(fliplr(I))];
        otherwise 
            error('Unsupported boundary option');
    end

    %Origin
    x0 = rows + 1;
    y0 = cols + 1;
    
    %Create the layers
    for d = 1:D
        xStart = x0 + displacements(d,1);
        yStart = y0 + displacements(d,2);
        xEnd = xStart + rows - 1;
        yEnd = yStart + cols - 1;
        L(:,:,d) = I(xStart:xEnd,yStart:yEnd);
    end
        
end