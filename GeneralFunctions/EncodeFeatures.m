function encodedData = EncodeFeatures(dataToEncode, encoder, optArgs)
%Encode a set of vectors using the encoder provided.
%This is a a wrapper around VLfeat's functions for BoW, Fisher vector and VLAD 
%encodings [1].
%**********
%Synopsis:          encodedFeats = EncodeFeatures(dataToEncode, encoder, encType)
%**********
%INPUT
%dataToEncode:      An N x M matrix where each column represents a point to
%                   encode. N is the dimension of the feature space.
%encode:            The encoder generated through GenerateFeatureEncoder().
%optArgs:           A cell of comma-separated optional arguments. The
%                   values can be:
%                       'Normalized', 'SquareRoot', 'Improved', 'Fast',
%                       'Verbose' 
%                           -- if encoder.type == 'Fisher' --
%                       'Unnormalized', 'NormalizeComponents',
%                       'NormalizeMass', 'SquareRoot', 'Verbose'
%                           -- if encoder.type == 'VLAD' --
%                   Pass [] if no optional argument is required
%**********
%OUTPUT:
%encodedData:       The encoded data. An F x 1 vector of encoded features.
%                       F = K   -- if encoder.type == 'BoW'
%                   	F = 2KD -- if encoder.type == 'Fisher'
%                       F = KD  -- if encoder.type == 'VLAD'
%                   Where D is the dimension of the feature space and K the
%                   number of visual words (encoder.nClusters)
%********** 
%Dependencies
%VLFeat: http://www.vlfeat.org/
%**********
%
%   Sample usage
%   --------
%      %Examples:
%      %Generate the train data and the data to encode
%      numTrainPoints = 100;
%      numPointsToEncode = 300;
%      dimension = 5;
%      trainData = rand(dimension, numTrainPoints);
%      dataToEncode = rand(dimension, numPointsToEncode);
%      nClusters = 20;
%
%      %Encode data using BoW
%      nClusters = 15;
%      encoder = GenerateFeatureEncoder(trainData, 'BoW', nClusters);
%      encodedData = EncodeFeatures(dataToEncode, encoder, []);
%
%      %Encode data using Fisher vector
%      nClusters = 20;
%      encoder = GenerateFeatureEncoder(trainData, 'Fisher', nClusters);
%      encodedData = EncodeFeatures(dataToEncode, encoder, []);
%
%      %Encode data using Fisher vector. 
%      %Optional arguments: 'Fast' and 'Improved' 
%      optArgs = {'Fast', 'Improved'};
%      encodedData = EncodeFeatures(dataToEncode, encoder, optArgs);
%
%      %Encode using VLAD
%      encoder = GenerateFeatureEncoder(trainData, 'VLAD', nClusters);
%      encodedData = EncodeFeatures(dataToEncode, encoder, []);
%
%      %Encode using VLAD
%      %Optional arguments: 'Unnormalized'
%      optArgs = {'Unnormalized'};
%      encodedData = EncodeFeatures(dataToEncode, encoder, []);
    encodedData = [];

    switch encoder.type
        case 'BoW'
            nn = vl_kdtreequery(encoder.kdtree,...
                                encoder.centers,... 
                                dataToEncode);
            encodedData = histc(nn(:),1:encoder.nclusters)/numel(nn);
        case 'Fisher'
            baseParams = {dataToEncode,... 
                          encoder.means,...
                          encoder.covariances,...
                          encoder.priors};
            params = [baseParams, optArgs];
            encodedData = vl_fisher(params{:});
        case 'VLAD'
            nn = vl_kdtreequery(encoder.kdtree,...
                                encoder.centers,... 
                                dataToEncode);
            assignments = single(zeros(encoder.nclusters,...
                                 size(dataToEncode,2)));
            assignments(sub2ind(size(assignments), nn, 1:length(nn))) = 1;
            baseParams = {dataToEncode,...
                          encoder.centers,...
                          assignments};
            params = [baseParams, optArgs];
            encodedData = vl_vlad(params{:});
        otherwise
            error('Encoding type unsupported');
    end
end

