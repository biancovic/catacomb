function Mosaic = CreateMosaic(path, width, height, num_width, space_width, space_height);
%Create a tiled image mosaic
%**********
%Synopsis:         Mosaic = CreateMosaic(path, width, height, num_width, space_width, space_height);
%**********
%INPUT:
%path:             Full path to the directory containing the input images 
%width:            Width of the mosaic tiles 
%height:           Height of the mosaic tiles 
%num_width:        Number of tiles in each column. 
%                  with white tiles.
%space_width:      Number of white pixels separating columns
%space_height:     Number of white pixels separating rows
%**********
%OUTPUT:
%Mosaic:           A mosaic with num_width tiles per column and N \ (num_width)
%                  tiles per row, being N the number of the input images. 
%                  The remainder mod(N,num_width) cells are filled with
%                  white tiles.
%**********

%Admitted extension
admittedExtensions = {'.bmp','.jpg','.png','.JPG'};

%Read the list of files in the directory
dirContents = dir(path);
nFiles = size(dirContents,1);

imageNames = GetFileNames(path);

nImages = size(imageNames,1);
fprintf('Number of images: %d\n', nImages);

imgFile = deblank(imageNames(1,:));
[rows cols channels] = size(imread(imgFile));

num_height = ceil(nImages/num_width);

%Create white image (background)
Mosaic = uint8(255*ones(height*num_height + space_height*(num_height - 1),width*num_width + space_width*(num_width - 1),channels));

%Fill the mosaic
imgCounter = 1;

for h = 1:num_height
    for w = 1:num_width
        if imgCounter > nImages
            break;
        end
        start_row = 1 + (height + space_height)*(h - 1);
        start_col = 1 + (width + space_width)*(w - 1);
        end_row = start_row + height - 1;
        end_col = start_col + width - 1;
        imgFile = deblank(imageNames(imgCounter,:));
        fprintf('Adding image to mosaic: %s\n', imgFile);
        %[pathstr,name,ext] = fileparts(imgFile); 
            
        [img, map] = imread(imgFile);
        resizedImage = imresize(img,[height width],'bicubic');
        Mosaic(start_row:end_row,start_col:end_col,:) = resizedImage;
        imgCounter = imgCounter + 1;
        end
    end
end


