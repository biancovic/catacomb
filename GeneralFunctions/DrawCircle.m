function [C] = DrawCircle(R,circleType)
%Digital circles
%**********
%Synopsis:      [C] = DrawCircle(R,circleType)
%**********
%INPUT
%R:             Radius (in pixels) of the circle
%circleType:    Type of algorithm used to draw the circle. Can be:
%                    'Andres'              -> Andres' digital circle []
%                    'eight-point-square'  -> Eight points on a square at 
%                                             0�, 45�,...315�
%                    'eight-point-circle'  -> Eight points on a circle at 
%**********                                             0�, 45�,...315�
%OUTPUT
%C:             The cartesian coordinates of the circle (N x 2 matrix).
%               It is assumed the center has coordinates (0,0)

%References
%[1] Andres, A. and Roussillon, T. Analytical Description of Digital Circles. 
%16th Discrete Geometry for Computer Imagery, Apr 2011, Nancy, France. Springer Verlag,
%6607, pp.235-246, 2011, Lecture Notes in Computer Science.

    nPoints = 0;
    C = [];
    
    switch circleType
        case 'Andres'
            nPoints = 2*ceil(2*pi*R); 
            angularStep = 2*pi/nPoints;
            for n = 1:nPoints
                angle = angularStep*(n-1);
                tc = [floor(R*cos(angle)) floor(R*sin(angle));...
                      ceil(R*cos(angle)) floor(R*sin(angle));...
                      floor(R*cos(angle)) ceil(R*sin(angle));
                      ceil(R*cos(angle)) ceil(R*sin(angle))];
                for i = 1:size(tc,1)
                    if ((tc(i,1)^2 + tc(i,2)^2) <= (R + 0.5)^2) && ...
                       ((tc(i,1)^2 + tc(i,2)^2) >= (R - 0.5)^2);
                        
                       %Add if not present
                       present = false;
                       for j = 1:size(C,1)
                           if (tc(i,1) == C(j,1)) && (tc(i,2) == C(j,2))
                               present = true;
                           end
                       end
                       if ~present
                        C = [C; tc(i,1), tc(i,2)];
                       end
                    end
                end
            end
        case 'eight-point-square'
            C = R*[-1 -1; 0 -1; 1 -1; 1 0; 1 1; 0 1; -1 1; -1 0];
        case 'eight-point-circle'
            nPoints = 8;
            angularStep = 2*pi/nPoints;
            for n = 1:nPoints
                angle = angularStep*(n-1);
                C = [C; round(R*cos(angle)), round(R*sin(angle))];
            end
        otherwise
                error('Unsupported algorithm for drawing circles');
    end
        
    %Reorder the points in anti-clockwise order
    angle = atan2(C(:,2),C(:,1));
    [foo,idx] = sort(angle,'ascend');
    C = C(idx,:);

end