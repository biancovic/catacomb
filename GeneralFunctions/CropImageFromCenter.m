function Iout = CropImageFromCenter(Iin, w, h)
%Crop image from the center
%**********
%Synopsis:  Iout = CropImageFromCenter(Iin, w, h)
%**********
%INPUT
%Iin:       Input image
%w:         New width
%h:         New height
%**********
%OUTPUT
%Iout:      Output image. This is a central crop of dimensions w x h of the
%           original image.
%
%  Sample usage
%  --------
%       I = imread('peppers.png');                           
%       Ic = CropImageFromCenter(I, 100, 100);


    [H W c] = size(Iin);
    
    if (w > W) || (h > H)
        error('Cropped image should be smaller than the original');
    end
    
    Iout = Iin(1+round((H-h)/2):round((H+h)/2),1+round((W-w)/2):round((W+w)/2),:);
end