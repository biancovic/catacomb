function featureVector = GLCMFeatures(img, d, nt, avg, fn, bitDepth)
%Implementation of grey-level co-occurrence features as proposed in Ref. [1]
%**********
%Synopsis:      featureVector = GLCMFeatures(img, d, nt, avg, fn, bitDepth)
%**********
%INPUT:
%img            The grey-scale input image
%d              The distance in pixels at which the co-occurrence matrices 
%               are computed   
%nt             The type of neighbourhood. Can be:
%                   'HAR'  Haralick's original implementation, i.e.: 
%                          four directions at 0�, 45�, 90� and 135�
%                          See Ref. [2].
%                   'PET'  Petrou's digital circles - see Ref. [3].
%avg            Whether the matrices should be averaged or not (true/false)
%fn             The type of normalization for obtaining rotation-invariant 
%               features. Can be
%                   'NONE'          -> no normalization (directional features) 
%                   'AVG'           -> average
%                   'AVG+RANGE'     -> average + range
%                   'AVG+RANGE+MAD' -> average + range + mean absolute deviation
%                   'DFT'           -> discrete Fourier transform
%bitDepth       The bit depth of the input image
%**********
%OUTPUT:
%featureVector  The feature vector
%**********
%
%   Sample usage
%   --------
%      %Distance = 2 pixels, neighbourhood type = digital circle,
%      %directional features
%      img = imread('rice.png');
%      bitDepth = 8;
%      f = GLCMFeatures(img, 1, 'PET', false, 'NONE', bitDepth);
%
%
%References
%[1] F. Bianconi and A. Fern�ndez, A.; "Rotation invariant co-occurrence 
%    features based on digital circles and discrete Fourier transform"
%    Pattern Recognition Letters, 48, pp. 34-41.
%[2] R. M. Haralick et al.; "Textural features for image classification." 
%    IEEE Transactions on Systems, Man, and Cybernetics, 3(6):610�621, 1973.
%[3] M. Petrou and P. Garc�a Sevilla; "Image Processing. 
%    Dealing with Texture." Wiley Interscience, 2006.

    
    %Compute the co-occurrence matrices
    CM = COOCMatrices(img, d, nt, avg, bitDepth);
    
    %Compute the features
    featureVector = ComputeCooccurrenceFeatures(CM, fn);
    
end



