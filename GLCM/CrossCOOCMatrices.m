function CM = CrossCOOCMatrices(img1, img2, d, nt, avg, bitDepth)
%Grey-level cross co-occurrence matrices. Can be used to compute integrative
%co-occurrence features
%**********
%Synopsis:      CM = CrossCOOCMatrices(img1, img2, d, nt, avg, bitDepth)
%**********
%INPUT
%img1:          The first gray-scale input image
%img2:          The second gray-scale input image
%d:             The distance at which the matrices are computed (integer)
%nt:            The type of neighbourhood
%                   can be:    'HAR'  Haralick's original implementation [1]
%                              'PET'  Petrou's digital circles [2]
%avg:           Average matrices. If true the co-occurrence matrices are averaged
%bitDepth:      The number of levels into which the image is quantised
%**********
%OUTPUT
%CM:            Stack of one (if avg == 'Y') or more co-occurrence matrices
%               (if avg ~= 'Y')
%**********
%
%  Sample usage
%  --------
%       %Compute the cross co-occurrence matrices at distance 1px between R 
%       and B channels using Haralick's displacements
%       I=imread('fabric.png');
%       R = I(:,:,1);
%       B = I(:,:,3);
%       bitDepth = 8; 
%       d = 1;
%       nt = 'HAR'
%       avg = false;
%       CM = CrossCOOCMatrices(R, B, d, nt, avg, bitDepth);
%
%References
%[1]    Haralick, R.M., Dinstein, I., Shanmugam, K.
%       Textural Features for Image Classification
%       (1973) IEEE Transactions on Systems, Man and Cybernetics, 
%       SMC-3 (6), pp. 610-621
%[2]    Petrou, M. and Garc�a Sevilla, P, Image Processing. 
%       Dealing with Texture." Wiley Interscience, 2006.
    
    if sum(size(img1) ~= size(img2)) > 0
        error('The two input images must have the same size');
    end

    %Displacements
    D = [];
    
    switch nt
        case 'HAR'
            D = [0 d; -d d; -d 0; -d -d];
        case 'PET'
            switch d
                case 1
                    D = [-1 1; 0 1; 1 1; 1 0];
                case 2
                    D = [-2 1; -1 2; 0 2; 1 2; 2 1; 2 0];
                case 3
                    D = [-3 1; -2 2; -1 3; 0 3; 1 3; 2 2; 3 1; 3 0];
                case 4
                    D = [-4 1; -4 2; -3 2; -3 3; -2 3; -2 4; -1 4; 0 4; 1 4; 2 4; 2 3; 3 3; 3 2; 4 2; 4 1; 4 0];
                otherwise
                error('Unsupported displacement');
            end
        otherwise
            error('Unsupported neighbourhood');
    end
        
    %Number of displacements
    nDisp = size(D, 1);
    
    %Compute matrices
    CM = ComputeCrossCooccurrenceMatrices(img1, img2, 2^(bitDepth), D, 'Y');
     
    %Average matrices if required
    if avg == 'Y'
        CM = mean(CM, 3);
    end
    
end


