function [ET] = Entropy(CM)

    %Computes entropy of a co-occurrence matrix
    %
    %INPUT:
    %CM: the co-occurrence matrix
    %
    %OUTPUT
    %ET: entropy (normalized between 0 and 1)
    %
    
    [rows cols] = size(CM);
    if (rows ~= cols)
        error('Co-occurrence matrix should be square');
    end 
    
    ET = 0;
    
    %Compute ENTROPY
    for r = 1:rows
        for c = 1:cols
            if CM(r,c) ~= 0
                ET = ET + CM(r,c)*log2((CM(r,c)));
            end
        end
    end

    %Normalize entropy (entropy is max when all the entries of the matrix are equally probable)
    ET = (-1/(2*log2(rows)))*ET;

end