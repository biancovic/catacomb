function [H] = Homogeneity(CM)

    %Computes homogeneity of a co-occurrence matrix
    %
    %INPUT:
    %CM: the co-occurrence matrix
    %
    %OUTPUT
    %H: homogeneity value (normalized between 0 and 1)
    %
    
    [rows cols] = size(CM);
    if (rows ~= cols)
        error('Co-occurrence matrix should be square');
    end 
    
    stats = GraycoProps(CM, 'Homogeneity');    
    H = stats.Homogeneity;
    
end
