function [CN] = Contrast(CM)

    %Computes contrast of a co-occurrence matrix
    %
    %INPUT:
    %CM: the co-occurrence matrix
    %
    %OUTPUT
    %CN: contrast value (normalized between 0 and 1)
    %
    
    [rows cols] = size(CM);
    if (rows ~= cols)
        error('Co-occurrence matrix should be square');
    end 
    
    stats = GraycoProps(CM, 'Contrast');    
    CN = (1/((rows-1)^2))*(stats.Contrast);

end