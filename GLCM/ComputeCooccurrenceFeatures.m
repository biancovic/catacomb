function featureVector = ComputeCooccurrenceFeatures(cm, fn)
%Co-occurrence features from a co-occurrence matrix. The features are:
%Contrast, Correlation, Energy, Entropy and Homogeneity
%**********
%Synopsis:      featureVector = ComputeCooccurrenceFeatures(cm, fn)
%**********
%INPUT:
%cm             Stack of co-occurrence matrices corresponding to displacement 
%               vectors arranged sequentially in clock-wise or counter-clockwise
%               order
%fn             The type of normalization for obtaining rotation-invariant 
%               features. Can be
%                   'NONE'          -> no normalization (directional features) 
%                   'AVG'           -> average
%                   'AVG+RANGE'     -> average + range
%                   'AVG+RANGE+MAD' -> average + range + mean absolute deviation
%                   'DFT'           -> discrete Fourier transform
%**********
%OUTPUT:
%featureVector  The feature vector
%**********

    %Compute the co-occurrence features
    featureVector = [];
    featureMatrix = [];
    for d = 1:size(cm,3)
        featureVector_ = [Contrast(cm(:,:,d)),... 
                          Correlation(cm(:,:,d)),... 
                          Energy(cm(:,:,d)),...
                          Entropy(cm(:,:,d)),...
                          Homogeneity(cm(:,:,d))];
        featureMatrix = [featureMatrix; featureVector_];
    end
    
    %Normalise the co-occurrence features through the selecte method
    switch fn
        case 'NONE'
            %Do nothing
            featureVector = reshape(featureMatrix, 1, numel(featureMatrix));
        case 'AVG'
            featureVector = mean(featureMatrix);
        case 'AVG+RANGE'
            featureVector = [mean(featureMatrix), max(featureMatrix) - min(featureMatrix)];
        case 'AVG+MAD'
            featureVector = [mean(featureMatrix), mad(featureMatrix)];
        case 'AVG+RANGE+MAD'
            featureVector = [mean(featureMatrix),...
                             max(featureMatrix) - min(featureMatrix),...
                             mad(featureMatrix)];
        case 'DFT'
            featureMatrix = abs(fft(featureMatrix, [], 1));
            featureVector = reshape(featureMatrix, 1, numel(featureMatrix));
        case 'DFT+RANGE'
            featureVector = ComputeCooccurrenceFeatures(cm, 'DFT');
            featureVector = [featureVector, max(featureMatrix) - min(featureMatrix)];            
        otherwise error('Unsupported method for feature normalization');
    end
end

