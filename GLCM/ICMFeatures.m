function featureVector = ICMFeatures(img, d, nt, avg, fn, bitDepth)
%Integrative co-occurrence features
%**********
%Synopsis:      featureVector = ICMFeatures(img, d, nt, avg, fn, bitDepth)
%**********
%INPUT:
%img            A three-channel colour input image
%d              The distance in pixels at which the co-occurrence matrices 
%               are computed   
%nt             The type of neighbourhood. Can be:
%                   'HAR'  Haralick's original implementation, i.e.: 
%                          four directions at 0�, 45�, 90� and 135�
%                          See Ref. [2].
%                   'PET'  Petrou's digital circles - see Ref. [3].
%avg            Whether the matrices should be averaged or not (true/false)
%fn             The type of normalization for obtaining rotation-invariant 
%               features. Can be
%                   'NONE'          -> no normalization (directional features) 
%                   'AVG'           -> average
%                   'AVG+RANGE'     -> average + range
%                   'AVG+RANGE+MAD' -> average + range + mean absolute deviation
%                   'DFT'           -> discrete Fourier transform
%bitDepth       The bit depth of the input image
%**********
%OUTPUT:
%featureVector  The feature vector
%**********
%
%   Sample usage
%   --------
%      %Example 1
%      %Distance = 2 pixels, neighbourhood type = digital circle,
%      %directional features
%      img = imread('fabric.png');
%      bitDepth = 8;
%      f = ICMFeatures(img, 1, 'PET', false, 'NONE', bitDepth);
%
%
%References
%[1] V. Arvis, C. Debain, M. Berducat and A. Benassi; "Generalization of 
%    the cooccurrence matrix for colour image analysis"; Image Analysis and
%    Stereology, 23(1)
%[2] F. Bianconi and A. Fern�ndez, A.; "Rotation invariant co-occurrence 
%    features based on digital circles and discrete Fourier transform"
%    Pattern Recognition Letters, 48, pp. 34-41.
%[3] R. M. Haralick et al.; "Textural features for image classification." 
%    IEEE Transactions on Systems, Man, and Cybernetics, 3(6):610�621, 1973.
%[4] M. Petrou and P. Garc�a Sevilla; "Image Processing. 
%    Dealing with Texture." Wiley Interscience, 2006.

    
    %Compute the intra-channel co-occurrence features
    featureVector = [GLCMFeatures(img(:,:,1), d, nt, avg, fn, bitDepth), ...
                     GLCMFeatures(img(:,:,2), d, nt, avg, fn, bitDepth), ...
                     GLCMFeatures(img(:,:,3), d, nt, avg, fn, bitDepth)];

    %Compute the inter-channel co-occurrence matrices
    CM_inter_RG = CrossCOOCMatrices(img(:,:,1), img(:,:,2), d, nt, avg, bitDepth);
    CM_inter_RB = CrossCOOCMatrices(img(:,:,1), img(:,:,3), d, nt, avg, bitDepth);
    CM_inter_GB = CrossCOOCMatrices(img(:,:,2), img(:,:,3), d, nt, avg, bitDepth);
    
    %Compute the inter-channel co-occurrence features
    featureVector = [featureVector, ...
                     ComputeCooccurrenceFeatures(CM_inter_RG, fn), ...
                     ComputeCooccurrenceFeatures(CM_inter_RB, fn), ...
                     ComputeCooccurrenceFeatures(CM_inter_GB, fn)];
    
end