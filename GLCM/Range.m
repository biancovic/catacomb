function [R] = Range(CM)

    %Computes the range of a co-occurrence matrix. This is the difference
    %between the minimum and maximum value.
    %
    %INPUT:
    %CM: the co-occurrence matrix
    %
    %OUTPUT
    %R: range. 
    %
    
    R = max(CM(:)) - min(CM(:));

end