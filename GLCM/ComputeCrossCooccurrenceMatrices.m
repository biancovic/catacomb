function CM = ComputeCrossCooccurrenceMatrices(I1, I2, G, displacements, symmetrize);
%Grey-level cross co-occurrence matrices between two intensity images given 
%a set of displacements. Filtering across borders is obtained through circular scan.
%Can be used to compute integrative co-occurrence features.
%**********
%Synopsis:      CM = ComputeCooccurrenceMatrices(I, G, displacements, symmetrize);
%**********
%INPUT:
%I1:            The first grey-scale image
%I2:            The second grey-scale image
%G:             Number of grey levels into which the two images are quantised 
%               (bit depth)
%displacements: Displacements at which the co-occurrence matrices are
%               computed (matrix of N x 2 integers)
%                   Example: d = [1 1] -> 1 pixel right / 1 pixel up
%                            d = [0 1] -> 0 pixel right / 1 pixel up
%                            d = [-1 0] -> 1 pixel left / 0 pixel up
%symmetrize:    Makes the CM symmetric if true
%**********
%OUTPUT
%CM:            Stack of N co-occurrence matrices (3D matrix - G x G x N)
%**********
%
%  Sample usage
%  --------
%       %Compute the cross co-occurrence matrix between R and G channels
%       I=imread('fabric.png');
%       R = I(:,:,1);
%       G = I(:,:,2);
%       bitDepth = 8; 
%       d = [0 1; 1 1];
%       symm = 'N';
%       CM = ComputeCrossCooccurrenceMatrices(R, G, 2^bitDepth, d, symm);


    [rows cols] = size(I1);
    
    if sum(size(I1) ~= size(I2)) > 0
        error('The two input images must have the same size');
    end

    nDisplacements = size(displacements, 1);

    %Compute layers
    [L] = CreateImageLayers(I2,displacements,'circular');
    
    %Convert to double (working with integers is troublesome)
    I = double(I1);
    L = double(L);
    
    CM = zeros(G, G, nDisplacements);

    for d = 1:nDisplacements
        COOC = I.*G + L(:,:,d);
        H = histc(COOC(:),0:G^2-1)/(rows * cols);
        CM(:,:,d) = reshape(H,G,G);
    end

    %Symmetrize matrices if required
    if symmetrize
        for n = 1:nDisplacements
            CM(:,:,n) = (CM(:,:,n) + CM(:,:,n)')/2;
        end
    end
end
