function [EN] = Energy(CM)

    %Computes energy of a co-occurrence matrix
    %
    %INPUT:
    %CM: the co-occurrence matrix
    %
    %OUTPUT
    %EN: energy (normalized between 0 and 1)
    %
    
    [rows cols] = size(CM);
    if (rows ~= cols)
        error('Co-occurrence matrix should be square');
    end 
    
    stats = GraycoProps(CM, 'Energy');    
    EN = stats.Energy;

end