function encoder = GeneratePoolingEncoderSIFT(trainImages, encType, ...
                                              nClusters, samplingStep, binSize)
%Pooling encoder for dense SIFT features
%**********
%Synopsis:      encoder = GeneratePoolingEncoderSIFT(trainImages, encType, ...
%                                                    nClusters, samplingStep, binSize)
%**********
%INPUT
%trainImages:   Relative or absolute path to the folder containing the 
%               train images.
%encType:       The type of encoding. Can be:
%                   'BoW'       -> Bag-of-words encoding
%                   'Fisher'    -> Fisher vector encoding (FV)
%                   'VLAD'      -> Vector of Linearly Aggregated
%                                  Descriptors
%nClusters:     Number of clusters used to encode the features (i.e. 
%               dimension of the dictionary)
%samplingStep:  The step used to sample the input image. If step = 1 the
%               image is sample densely (i.e. at each pixel)
%binSize:       The width in pixel of a spatial bin. 
%**********
%OUTPUT
%encoder:       The structure containing the data for the encoding. 
%               See GenerateFeatureEncoder()
%
%   Sample usage
%   --------
%      %Example: Create a bag-of-words vector encoder
%      trainImages = './MyImages';                  %Define the image repository
%      nClusters = 16;                              %Set the number of clusters                       
%      samplingStep = 2;                            %Set the step   
%      binSize = 6;                                 %Set the bins' radius
%      encoder = GeneratePoolingEncoderSIFT(trainImages, 'BoW', ...
%                                           nClusters, samplingStep, binSize);

    trainData = single([]);

    %Get the train images
    images = GetFileNames(trainImages);
    nTrainImages = size(images, 1);
    
    %Number of clusters for image
    nClustersPerImage = ones(1,nTrainImages);
    if nTrainImages > nClusters
        %Do nothing here -- see below
    else
        r = mod(nClusters, nTrainImages);
        n = (nClusters - r)/nTrainImages;
        nClustersPerImage = n*nClustersPerImage;
        nClustersPerImage(end) = nClustersPerImage(end) + r;
    end
    
    %Process the train images and collect the train data
    centers = [];
    means = [];
    covariances = [];
    priors = [];
    nImages = size(images,1);
    for i = 1:nImages
        img = [];
        imgFile = images(i,:);
        img = imread(imgFile);
        [rows cols ch] = size(img);
        
        %Convert to greyscale if required
        if ch > 1
            img = rgb2gray(img);
        end
        
        fprintf('Processing image %d of %d: %s\n', i, nImages, imgFile);
        
        %convert to single precision
        img = single(img);
        
        %Compute dense sift
        [~,X] = vl_dsift(img, 'step', samplingStep,...
                              'size', binSize,...
                              'fast'); 
        X = single(X);
        
        %Generate the encoders' data
        encoder = GenerateFeatureEncoder(X, encType, nClustersPerImage(i));   
        
        switch encType
            case 'BoW'
                centers_ = vl_kmeans(X, nClustersPerImage(i));
                centers = [centers, centers_];
            case 'Fisher'
                [centers_, covariances_, priors_] = ...
                    vl_gmm(X, nClustersPerImage(i));     
                centers = [centers, centers_];
                covariances = [covariances, covariances_];
                priors = [priors, priors_];
            case 'VLAD'
                centers_ = vl_kmeans(X, nClustersPerImage(i));
                centers = [centers, centers_];
            otherwise 
                error('Unsupported encoding');
        end
        a = 0;
    end
    
    %If nTrainImages < nClusters use the cluster centers as train data
    encoder = [];
    if nTrainImages > nClusters
        encoder = GenerateFeatureEncoder(centers, encType, nClusters);
    else
        switch encType
            case 'BoW'            
                %Define the the data-to-cluster assignments through a kd-tree
                kdtree = vl_kdtreebuild(centers) ;      
                encoder = struct('type', 'BoW',...
                                 'nclusters', nClusters,...
                                 'centers', centers,...
                                 'kdtree', kdtree);
            case 'Fisher'    
                encoder = struct('type', 'Fisher',...
                                 'nclusters', nClusters,...
                                 'means', means,...
                                 'covariances', covariances,...
                                 'priors', priors);
            case 'VLAD'            
                kdtree = vl_kdtreebuild(centers) ;      
                encoder = struct('type', 'VLAD',...
                                 'nclusters', nClusters,...
                                 'centers', centers,...
                                 'kdtree', kdtree);
        otherwise 
            error('Unsupported encoding');
        end
    end
end

