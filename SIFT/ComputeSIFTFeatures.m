function F = ComputeSIFTFeatures(img, samplingStep, binSize, encoder, optArgs)
%Image features based on Convolutional Neural Networks
%**********
%Synopsis:         F = ComputeSIFTFeatures(img, samplingStep, binSize, encoder, optArgs)
%**********
%INPUT:
%img:              The input image
%samplingStep:     The step used to sample the input image. If step = 1 the
%                  image is sample densely (i.e. at each pixel)
%binSize:          The width in pixel of a spatial bin. 
%encoder:          A pre-trained encoder -- see GeneratePoolingEncoderSIFT().
%optArgs:          Optional arguments for feature encoding -- see
%                  EncodeFeatures().
%**********
%OUTPUT:
%features:         The feature vector.  
%**********
%
%   Sample usage
%   --------
%
%      %Example 1: Compute dense SIFT features with BoW pooling
%      img = imread('peppers.png');               %Load the image
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 25;                            %Number of clusters 
%      samplingStep = 3;                          %Sampling step
%      binSize = 6;                               %Bin radius
%      encType = 'BoW';                           %Type of encoder
%      BoWEncoder = GeneratePoolingEncoderSIFT(trainImages, encType, ...
%                                              nClusters, samplingStep, binSize)
%      F = ComputeSIFTFeatures(img, encoder, []);
%
%      %Example 2: Compute dense SIFT features with improved Fisher vector pooling
%      img = imread('peppers.png');               %Load the image
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 55;                            %Number of clusters 
%      samplingStep = 3;                          %Sampling step
%      binSize = 6;                               %Bin radius
%      encType = 'Fisher';                        %Type of encoder  
%      IVFEncoder = GeneratePoolingEncoderSIFT(trainImages, encType, ...
%                                              nClusters, samplingStep, ...
%                                              binSize);
%      optArgs = {'Improved'};
%      F = ComputeSIFTFeatures(img, IVFEncoder, optArgs);
%
%      %Example 3: Compute dense SIFT features pooled with VLAD and individually
%      L2-normalised subvectors.
%      img = imread('peppers.png');               %Load the image
%      trainImages = './myImages';                %Define the image repository
%      nClusters = 10;                            %Number of clusters 
%      samplingStep = 3;                          %Sampling step
%      binSize = 6;                               %Bin radius
%      encType = 'VLAD';                          %Type of encoder  
%      VLADEncoder = GeneratePoolingEncoderSIFT(trainImages, encType, ...
%                                              nClusters, samplingStep, ...
%                                              binSize);
%      optArgs = {'NormalizeComponents'};
%      F = ComputeSIFTFeatures(img, VLADEncoder, optArgs);

    [rows cols ch] = size(img);
    
    %Convert to grey-scale if required
    if ch >  1
        img = rgb2gray(img);
    end

    %convert to single precision
    img = single(img);
        
    %Process the image through dense SIFT
    [~,X] = vl_dsift(img, 'step', samplingStep,...
                          'size', binSize,...
                          'fast');
    
    %Encode the features
    F = EncodeFeatures(single(X), encoder, optArgs);    
    F = F';
end



