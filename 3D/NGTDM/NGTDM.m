function DM = NGTDM(V, ROI, numLevels, displacements, filtering);
%Three-dimensional neighbourhood grey-tone difference matrix
%**********
%Synopsis:      NGTDM(V, ROI, numLevels, displacements, filtering);
%**********
%INPUT:
%V:             A 3D matrix of integer in the [0,numLevels-1] interval.  
%ROI:           A 3D matrix of logical (mask) representing the volume of 
%               interest within V. Same size as V.
%numLevels:     Number of levels into which V is quantised (bit depth)
%displacements: Set of displacements defining the neighbourhood over which 
%               the difference matrices are computed (matrix of N x 3 integers). 
%               The convention is:
%                   1st coordinate -> from top to bottom
%                   2nd coordinate -> from left to right
%                   3rd coordinate -> from the first layer to the others
%                   Example: d = [x y z] -> x pixels down 
%                                           y pixels right
%                                           z pixels deep
%               The displacement [0 0 0] (central voxel) should not be
%               included.
%filtering:     How the co-occurrences are computed across borders. 
%               Possible values:
%                   'circular'
%                   'symmetric'
%               See also CreateImageLayers3D()
%**********
%OUTPUT
%DM:            The difference matrix, same size as V
%**********
%
%  Sample usage
%  --------
%       %Create a 3D volume with random values
%       V = rand([50 50 50]);   
%       %Create a random ROI
%       ROI = round(rand(size(V)));
%       Vmax = max(V(:));
%       Vmin = min(V(:));
%       numLevels = 64;
%       %Quantize into numLevels levels
%       V = uint8(round((numLevels-1)*(V - Vmin)/(Vmax - Vmin)));
%       %Define the displacement vectors
%       disps = [1 0 0; 0 1 0; 1 1 1];  
%       DM = NGTDM(V, ROI, numLevels, disps, 'circular');

%References
%[1]    Adamasun, M. and King, R.; Textural features corresponding to
%       textural properties, IEEE Transactions on Systems, Man and Cybernetics
%       19(5):1264-1274

    nDisplacements = size(displacements, 1);

    %Compute layers
    L = CreateImageLayers3D(V, displacements, filtering);
        
    %Compute the average value of the voxels around the central pixel
    A = mean(L,4);
    
    %Compute the NGTDM entry of each intensity level
    for i = 0:numLevels-1
        validIndicesThisLevel = find((V == i) .* ROI);
        si = abs(i - A(validIndicesThisLevel));
        N = numel(validIndicesThisLevel);
        if N > 0
            DM(i+1) = sum(si(:))/N;
        else
            DM(i+1) = 0;
        end
        %NOTE: the normalisation factor *numel(validIndicesThisLevel)* is
        %not present in [1], but is required to guarantee that the
        %resulting features are independent on the overall number of voxels
    end

end
