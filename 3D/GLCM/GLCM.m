function CM = GLCM(V, ROI, numLevels, displacements, filtering);
%Three-dimensional grey-level co-occurrence matrices
%**********
%Synopsis:      CM = GLCM(V, ROI, numLevels, displacements, filtering);
%**********
%INPUT:
%V:             A 3D matrix of integer in the [0,numLevels-1] interval.
%ROI:           A 3D matrix of logical (mask) representing the volume of 
%               interest within V. Same size as V.
%numLevels:     Number of levels into which V is quantised (bit depth).
%displacements: Displacements at which the co-occurrence matrices are
%               computed (matrix of N x 3 integers). Convention is:
%                   1st coordinate -> from top to bottom
%                   2nd coordinate -> from left to right
%                   3rd coordinate -> from the first layer to the others
%                   Example: d = [x y z] -> x pixels down 
%                                           y pixels right
%                                           z pixels deep
%filtering:     How the co-occurrences are computed across borders. 
%               Possible values:
%                   'circular'
%                   'symmetric'
%               See also CreateImageLayers3D()
%**********
%OUTPUT
%CM:            Stack of N co-occurrence matrices of dimension numLevels x numLevels each
%**********
%
%  Sample usage
%  --------
%       %Create a 3D volume with random values
%       V = rand([50 50 50]);   
%       %Create a random ROI
%       ROI = round(rand(size(V)));
%       Vmax = max(V(:));
%       Vmin = min(V(:));
%       numLevels = 64;
%       %Quantize into numLevels levels
%       V = uint8(round((numLevels-1)*(V - Vmin)/(Vmax - Vmin)));
%       %Define the displacement vectors
%       disps = [1 0 0; 0 1 0; 1 1 1];                  
%       CM = GLCM(V, ROI, numLevels, disps, 'circular');

    nDisplacements = size(displacements, 1);

    %Compute layers
    L = CreateImageLayers3D(V, displacements, filtering);
    
    %Convert to double to avoid overflow problema
    V = double(V);
    L = double(L);
    
    CM = zeros(numLevels, numLevels, nDisplacements);
    
    for d = 1:nDisplacements
        %Only compute those co-occurrences for which the value of both
        %voxels is different than -1
        validIndices = find(ROI & (L(:,:,:,d) ~= -1));
        N = numel(validIndices);
        
        COOC = V.*numLevels + L(:,:,:,d);
        H = hist(COOC(validIndices),0:numLevels^2-1)/N;
        CM(:,:,d) = reshape(H,numLevels,numLevels);
    end
end
