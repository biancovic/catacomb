function GLRL = GLRLM(V, ROI, numLevels, directions, filtering);
%Three-dimensional grey-level run-length matrices
%**********
%Synopsis:      GLRL = GLRLM(V, ROI, numLevels, displacements, filtering);
%**********
%INPUT:
%V:             A 3D matrix of integers in the range [0,numLevels - 1]. 
%numLevels:     Number of levels into which V is quantised (bit depth)
%ROI:           A 3D matrix of logical representing the volume of interest.
%               Same size as V.
%directions:    Directions along which the run-length matrices are
%               computed (matrix of N x 3 integers). Convention is:
%                   1st coordinate -> from top to bottom
%                   2nd coordinate -> from left to right
%                   3rd coordinate -> from the first layer to the others
%                   Example: d = [x y z] -> x pixels down 
%                                           y pixels right
%                                           z pixels deep
%               Each x, y and z can be either 0, 1 or -1
%filtering:     How the co-occurrences are computed across borders. 
%               Possible values:
%                   'circular'
%                   'symmetric'
%               See also CreateImageLayers3D()
%**********
%OUTPUT
%GLRL:          Stack of N run-length matrices. The dimension of each
%               matrix is min(size(V)) x numLevels. The matrices are
%               not normalised to sum one.
%**********
%
%  Sample usage
%  --------
%       %Create a 3D volume with random values
%       V = rand([50 50 50]);   
%       %Create a random ROI
%       ROI = round(rand(size(V)));
%       Vmax = max(V(:));
%       Vmin = min(V(:));
%       numLevels = 64;
%       %Quantize into numLevels levels
%       V = uint8(round((numLevels-1)*(V - Vmin)/(Vmax - Vmin)));
%       %Define the displacement vectors
%       directions = [1 0 0; 0 1 0; 1 1 1];                  
%       GLRL = GLRLM(V, ROI, numLevels, directions, 'circular');

%References
%[1]    Galloway, M.M.; Texture analysis using run lengths, Computer Graphics 
%       and Image Processing 4(2):172-179, 1975
      
    GLRL = [];

    nDirections = size(directions, 1);
    
    %The run lenght ranges from 1 to the length of the shortest side of V
    maxLength = min(size(V));

    runLengths = zeros([size(V) nDirections]);
    
    for d = 1:nDirections
        
        %Compute the displacement vectors, i.e. direction * length
        displacements = repmat((1:maxLength-1)', [1 size(directions,2)]) .* ...
                        repmat(directions(d,:), [maxLength-1 1]);
        
        %Do the slicing
        L = CreateImageLayers3D(V, displacements, filtering);
        
        %Initialize the runs
        runs = zeros([size(V) maxLength]);
        
        %Start with a mask of ones, then set '0' when the run stops
        mask = ones(size(V));
        runs(:,:,:,1) = mask;
        for l = 1:size(displacements,1)
            
            %Update the mask and exit if all the runs have stopped
            if l == 1
                mask = (L(:,:,:,l) == V) .* mask;
            else
                mask = (L(:,:,:,l-1) == L(:,:,:,l)) .* mask;
            end
            if ~(sum(mask(:)) > 0)
                break;
            end
            
            %Update the runs
            runs(:,:,:,l+1) = mask;

        end
        
        %Compute the run length
        runLengths(:,:,:,d) = sum(runs,4);
        
        %Compute the run-length matrices
        validIndices = find(ROI);
        N = numel(validIndices);
        RL = double(V).*maxLength + runLengths(:,:,:,d);
        H = hist(RL(validIndices),1:numLevels*maxLength);
        GLRL = cat(3, GLRL, reshape(H,maxLength,numLevels)');
        
    end
end
