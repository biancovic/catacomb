function [G] = GradientDirections(a,ndir)

%Funci�n que asigna a un �ngulo entre -pi y pi, un valor de 0 a n
%divisiones en la que se vaya a dividir el hisograma.

%Se obtienen dimensiones de la matriz de �ngulos y se inicializa la nueva
%con NaN, en el caso de que la matriz de �ngulos los tuviera.

    [m l] = size (a);
    G = NaN(m,l);
    
%Los NaN se acabar�n sustituyendo por el par�metro deseado de 0 a ndir, si
%procede:

    for n=1:(ndir-1)
        
        x=-pi+(n-1)*2*pi/(ndir-1);
        y=(-pi+2*pi/(ndir-1))+(n-1)*2*pi/(ndir-1);
        G((a>=x) & (a<y) & (~isnan(a)))=n-1;
        G((a==pi)& (~isnan(a)))=ndir-1;
        
%         fprintf('%f %f\n', x, y);
        
    end
    

    
    edges = 0:1:(ndir-1);
    vector = reshape(G,1,m*l);
    numNaN = sum(isnan(vector));
    if(numNaN == m*l)
        H=0;
    else
        H = histc(vector,edges)/(m*l-numNaN);
%         figure;
%         bar(edges,H);
    end
    
end